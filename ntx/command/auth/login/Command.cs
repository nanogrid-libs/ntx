﻿using System.Threading;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading.Tasks;
using ntx.api.auth;
using System;
using System.IO;
using ntx.api.io;
using System.Text;

namespace ntx.command.auth.login
{
    class Command : ICommand
    {
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {

            command.Description = "login into nanogrid";
            command.HelpOption("-h|--help");
            var username = command.Option(@"-u|--username",
            "username/email",
            CommandOptionType.SingleValue
            );
            var password = command.Option(@"-p|--password <prompt>",
            "password",
            CommandOptionType.SingleValue
            );

            var audience = command.Option(@"-a|--audience",
            "audience",
            CommandOptionType.SingleValue
            );

            var refreshOption = command.Option(@"-r|--refresh",
            "store refresh token locally",
            CommandOptionType.NoValue
            );

            var outputUriOption = command.Option("-o|--output <->",
                "output url",
                CommandOptionType.SingleValue
                );
            command.OnExecute(() =>
            {
                username.MustSetValue(command);
                audience.MustSetValue(command);
                options.Command = new Command(command)
                {
                    UserName = username.Value(),
                    Password = password.GetValueOrDefault(),
                    Audience =audience.Value(),
                    GlobalOptions = options,
                    WithRefresh = refreshOption.HasValue(),
                    OutputUriOption = outputUriOption.GetValueOrDefault()
                };
                return 0;
            });
        }

        private CommandLineOptions GlobalOptions { get; set; }
        private readonly CommandLineApplication _app;
        private bool WithRefresh { get; set; }
        private string OutputUriOption { get; set; }
        private string UserName { get; set; }
        private string Password { get; set; }
        private string Audience { get; set; }

        public Command(CommandLineApplication app)
        {
            _app = app;
            
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            if (Password == "prompt")
            {
                Console.Error.WriteLine("Enter password:");
                Password = api.util.Utils.ReadLineMasked();
                Console.Error.WriteLine();
            }
            var outputStream = LazyStream.Output(OutputUriOption, "text/plain",breaker);
            var token = await Authorization.LoginWithPassword(UserName, Password, Audience, WithRefresh);
            using (var w = new StreamWriter(outputStream, new UTF8Encoding(false)))
            {
                await w.WriteLineAsync(token);
            }
            return 0;
        }
    }
}
