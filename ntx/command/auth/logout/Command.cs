﻿using System.Threading;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading.Tasks;
using ntx.api.auth;
using System;
using System.IO;
using ntx.api.io;
using System.Text;

namespace ntx.command.auth.logout
{
    class Command : ICommand
    {
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {

            command.Description = "logout with current token";
            command.HelpOption("-h|--help");
            var refreshOption = command.Option(@"-r|--refresh",
             "also revoke refresh token",
             CommandOptionType.NoValue
             );

            command.OnExecute(() =>
            {
                options.Command = new Command(command)
                {
                    GlobalOptions = options,
                    WithRefresh = refreshOption.HasValue(),
                };
                return 0;
            });
        }

        private CommandLineOptions GlobalOptions { get; set; }
        private bool WithRefresh { get; set; }
        private readonly CommandLineApplication _app;

        public Command(CommandLineApplication app)
        {
            _app = app;
            
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {

            //stream with valid token
            await Authorization.Logout(GlobalOptions.TaskAuthToken, WithRefresh);
            
            return 0;
        }
    }
}
