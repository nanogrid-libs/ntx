﻿using System.Threading;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading.Tasks;
using ntx.api.auth;
using System;
using System.IO;
using ntx.api.io;

namespace ntx.command.auth.info
{
    class Command : ICommand
    {
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {

            command.Description = "print auth info";
            command.HelpOption("-h|--help");
            var refreshOption = command.Option(@"-r|--refresh",
            "refresh token info",
            CommandOptionType.NoValue
            );
            var outputUriOption = command.Option("-o|--output <->",
                "output url",
                CommandOptionType.SingleValue
                );
            command.OnExecute(() =>
            {
                options.Command = new Command(command)
                {
                    GlobalOptions = options,
                    WithRefresh = refreshOption.HasValue(),
                    OutputUriOption = outputUriOption.HasValue() ? outputUriOption.Value() : outputUriOption.ValueName
                };
                return 0;
            });
        }

        private CommandLineOptions GlobalOptions { get; set; }
        private readonly CommandLineApplication _app;
        private bool WithRefresh { get; set; }
        private string OutputUriOption { get; set; }
        public Command(CommandLineApplication app)
        {
            _app = app;
            
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var outputResolver = LazyStream.Output(OutputUriOption, "text/plain",breaker);
            //stream with valid token
            var tokenStream = LazyStream.Input(GlobalOptions.TaskAuthToken);
            //valid access token generator
            var accessTokenFactory = Authorization.AccesTokenFactory(tokenStream);
            await Authorization.PrintToken(accessTokenFactory, outputResolver);
            return 0;
        }
    }
}
