﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using Dasync.Collections;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using ntx.api.auth;
using ntx.api.pipe.events;
using ntx.api;
using ntx.api.pipe.tasks;
using Microsoft.Extensions.Logging;
using ntx.api.util;
using System.Threading.Tasks;
using System.IO;

namespace ntx.command.devel.batch
{
    class Command : ICommand
    {
        const string serviceId = "ntx.v2t.engine.EngineService";
        private static ILogger _logger = Logging.LoggerFactory.CreateLogger(serviceId);
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "streaming media transcription client";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText = 
                "Consumes: binary audio stream" 
                + Environment.NewLine 
                + "Produces: ntx.v2t.engine.Events";
            var inputUriOption = command.Option("-i|--input",
                "input filelist url input|output per line", 
                CommandOptionType.SingleValue
                );
            
            var labelOption = command.Option("-l|--label",
                "task label => see: ntx task list",
                CommandOptionType.SingleValue
                );
            var tagsOption = command.Option(@"-t|--tags",
              "task tags => see:  ntx task list",
              CommandOptionType.MultipleValue
              );
            var audioFormatOption = command.Option("-a|--audioformat <auto:0>",
                "format auto:$probesizeBytes|pcm:$sampleFormat:$sampleRate:$channelLayout",
                CommandOptionType.SingleValue
                );
            var chunkSizeOption = command.Option($"-c|--chunkSize <4096>",
               "input chunk size in bytes",
               CommandOptionType.SingleValue
               );
            var lexiconOption = command.Option($"--lexicon <none>",
              "user lexicon uri",
              CommandOptionType.SingleValue
              );
            
            var suffixOption = command.Option($"--suffix <.res>",
              "default suffix",
              CommandOptionType.SingleValue
              );
            var parallelism = command.Option("-p|--parallelism <1>"
              , "use parallelism, one zero means streaming"
             , CommandOptionType.SingleValue);

            var retryOption = command.Option("--retry <5:1000:1.5>"
               , "retry with exponencial backof count:initDelayMs:multiplier"
              , CommandOptionType.SingleValue);
            var channelOption = command.Option("--channel <downmix>"
               , "choose audio channel, dowmix|left|right"
              , CommandOptionType.SingleValue);

            var safeOption = command.Option("--safe"
               , "be safe"
              , CommandOptionType.NoValue);

            var taskUri = command.Option("-t|--task"
               , "use custom task"
              , CommandOptionType.SingleValue);

            var endpointOption = command.Option($"-e|--endpoint <http://localhost:6666>",
                "service endpoint => see: ntx auth info",
                CommandOptionType.SingleValue
                );

            command.OnExecute(() =>
            {
                if(!inputUriOption.HasValue())
                {
                    command.Error.WriteLine($"Missing: {inputUriOption.Description}");
                    command.ShowHelp();
                    return 1;
                }

                inputUriOption.MustSetValue(command);
                labelOption.MustSetValue(command);

                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    LabelOption = labelOption.GetValueOrDefault(),
                    TaskUri = taskUri.HasValue() ? taskUri.Value() : null,
                    EndpointOption = endpointOption.GetValueOrDefault(),
                    AudioFormatOption = audioFormatOption.GetValueOrDefault(),
                    ChunkSizeBytes = uint.Parse(chunkSizeOption.GetValueOrDefault()),
                    GlobalOptions = options,
                    LexiconUriOption = lexiconOption.GetValueOrDefault(),
                    Paralelism = int.Parse(parallelism.GetValueOrDefault()),
                    Retry = retryOption.GetValueOrDefault(),
                    ChannelOption = channelOption.GetValueOrDefault(),
                    SuffixOption = suffixOption.GetValueOrDefault(),
                    SafeOption = safeOption.HasValue(),
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string SuffixOption { get; set; }
        private string LabelOption { get; set; }
        private string AudioFormatOption { get; set; }
        private string EndpointOption { get; set; }
        private CommandLineOptions GlobalOptions { get; set; }
        private string LexiconUriOption { get; set; }
        private int Paralelism { get; set; }
        private string Retry { get; set; }
        private string ChannelOption { get; set; }
        private uint ChunkSizeBytes { get; set; }
        private string TaskUri { get; set; }

        private bool SafeOption { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async System.Threading.Tasks.Task<int> RunAsync(CancellationToken breaker)
        {
            var retry = RetryWithBackoff.ParseFromCmd(Retry);

            


            //stream with valid token
            var tokenStream = LazyStream.Input(GlobalOptions.TaskAuthToken);
            //valid access token generator
            var accessTokenFactory = Authorization.AccesTokenFactory(tokenStream);
            //select task and authorize it 

            var authorizedTaskFactory = TaskUri == null ? null :

               (await new cz.ntx.proto.scheduler.TaskConfiguration().
               ParseFromStream(LazyStream.Input(TaskUri))).
               ToAuthorizedTaskFactory(accessTokenFactory, LabelOption);

            //Print task info

            var liststream = LazyStream.Input(InputUriOption,breaker);


            
            //sevice Uri
            var endpointUri = authorizedTaskFactory == null ? new Uri(EndpointOption) : new Uri((await accessTokenFactory()).ServiceEndpoint);
            //Create grpc channel, channel can be used for multiple clients of various services
            var channel = endpointUri.ToGRPCChannel();

            //Fill v2t config
            var engineContext = new EngineContext()
                .ParseCmd(LabelOption,ChannelOption);
            engineContext.AudioFormat = new AudioFormat()
                .ParseCmd(AudioFormatOption); ;

            //Add lexicon if any
            await engineContext.AddLexiconFromUri(LexiconUriOption);
            //Create client on channel

            
            var bc = new System.Collections.Concurrent.BlockingCollection <Tuple<string,string>>(10000);
            bool textInput = engineContext.ConfigCase == EngineContext.ConfigOneofCase.Ppc;
            var tlist = new Task[Paralelism];
            for(var i = 0; i < Paralelism; i++)
            {
                tlist[i] = Task.Run(async () =>
                {
                    var client = new cz.ntx.proto.v2t.engine.EngineService.EngineServiceClient(channel);
                    var _retry = retry.Clone();
                    var _enginecontext = engineContext.Clone();
                    while (true)
                    {
                        Tuple<string, string> io = null;
                        try
                        {

                            io = bc.Take();
                            //Select reader
                            
                        }
                        catch (InvalidOperationException)
                        {
                            break;
                        }
                        _retry.Reset();
                        while (true)
                        {
                            try
                            {
                                _logger.LogInformation($"Processing {io.Item1} => {io.Item2}");

                                using var inputStreamResolver = LazyStream.Input(io.Item1, breaker);
                                var _out = io.Item2;
                                if (SafeOption) {
                                    _out = io.Item2 + ".tmp";
                                }
                                using var outputResolver = LazyStream.Output(_out, "application/json", breaker);
                                //Create pipe source
                                var eventsSource = textInput ?
                                    PipeSource.FromProtoJsonStream<Events>(inputStreamResolver, breaker)
                                    .ViaEventsLimiterByCount(10)
                                    : PipeSource.EventsFromBinaryStream(inputStreamResolver, (int)ChunkSizeBytes, breaker);
                                //Create sink from output stream
                                var sink = PipeSink.ProtoJsonStream<Events>(
                                     outputResolver
                                    );
                                await eventsSource.ViaV2TStreaming(client, engineContext, authorizedTaskFactory, breaker).RunWithSink(sink);
                                if (SafeOption)
                                {
                                    outputResolver.Close();
                                    File.Move(_out, io.Item2,true);
                                    
                                }
                                break;
                            }
                            catch (Exception ex)
                            {
                                _logger.LogWarning($"Failed {i}: {ex.Message}");
                                if (breaker.IsCancellationRequested)
                                {
                                    throw;
                                }
                                if (await _retry.Next(breaker))
                                    throw;
                            }
                        }
                    }
                }
                );
            }
            
            


            var pathSource = PipeSource.LinesFromText(liststream, breaker);

            await pathSource.ForEachAsync(x => {
                if (x.Contains("|")) {
                    var s = x.Split("|", 2);
                    bc.Add(new Tuple<string, string>(s[0], s[1]),breaker);
                }
                else
                {
                    bc.Add(new Tuple<string, string>(x, x+SuffixOption),breaker);
                }
            }
            );
            bc.CompleteAdding();
            await Task.WhenAll(tlist);
         
            return 0;
        }


    }
}
