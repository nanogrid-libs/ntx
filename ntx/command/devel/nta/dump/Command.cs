﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using System.IO;
using ntx.api.pipe.tasks;
using System.Threading.Tasks;
using cz.ntx.proto.v2t.misc;
using ntx.api;
using ntx.api.pipe.events;
using ntx.api.util;

namespace ntx.command.devel.nta.dump
{
    class Command : ICommand
    {

        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "nta helper tool";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes: nta file"
             + Environment.NewLine
             + "Produces: raw text"
             + Environment.NewLine + Environment.NewLine
             + "Filters: " + Environment.NewLine
             + " noise:$timeSeconds:$symbolToProduce - nonspeech breaking filter" + Environment.NewLine;
            var inputUriOption = command.Option("-i|--input",
                "input nta url",
                CommandOptionType.SingleValue
                );
            var outputUriOption = command.Option("-o|--output <->",
                "output text url",
                CommandOptionType.SingleValue
                );
            var outputAudioUriOption = command.Option("-a|--audio <none>",
                "output audio url",
                CommandOptionType.SingleValue
                );

            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    OutputAudioUriOption = outputAudioUriOption.GetValueOrDefault()
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private string OutputAudioUriOption { get; set; }
        private string FilterOption { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var input = LazyStream.Input(InputUriOption, breaker);


            var streams = NtaHelper.FetchStreamsFromTar(input, "^delta.json|audio.oga$");
            if (!streams.ContainsKey("delta.json"))
                throw new Exception("Missing delta stream");
            bool dumpAudio = OutputAudioUriOption != "none";
            if(dumpAudio && !streams.ContainsKey("audio.oga"))
            {
                throw new Exception("Missing audio stream");
            }

            input.Dispose();
            var reference = (await QuillDeltaContents.FromStream(streams["delta.json"])).ToRawTextStream();
            var output = LazyStream.Output(OutputUriOption, "application/json", breaker);
            await reference.CopyToAsync(output);
            await output.FlushAsync();
            if (dumpAudio)
            {
                var audio = LazyStream.Output(OutputAudioUriOption, "audio/ogg", breaker);
                await streams["audio.oga"].CopyToAsync(audio);
                await audio.FlushAsync();
            }
            return 0;
        }
    }
}