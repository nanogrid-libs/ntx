﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using Dasync.Collections;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using System.IO;
using ntx.api.auth;
using ntx.api.pipe.events;
using ntx.api;
using ntx.api.pipe.tasks;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using ntx.api.util;
using System.Text;

namespace ntx.command.devel.task.repo.build
{
    class Command : ICommand
    {
        private static ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.command.devel.task.repo.build");
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "task repo builder";
            command.HelpOption("-h|--help");
            var inputUriOption = command.Option("-i|--input", 
                "input taskdef url", 
                CommandOptionType.SingleValue
                );
            var outputUriOption = command.Option("-o|--output <->",
                "output index url", 
                CommandOptionType.SingleValue
                );
            var noWriteTasks= command.Option("--notasks",
                "dont write compiled tasks",
                CommandOptionType.NoValue
                );
            var templateExtension = command.Option("--template <.template.json>",
                "template suffix",
                CommandOptionType.NoValue
                );
            var interpolatedExtension = command.Option("--template <.json>",
                "interpolated suffix",
                CommandOptionType.NoValue
                );
            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    GlobalOptions = options,
                    ReadOnly=noWriteTasks.HasValue(),
                    TemplateSuffix= templateExtension.GetValueOrDefault(),
                    InterpolatedSuffix = interpolatedExtension.GetValueOrDefault()
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private bool ReadOnly { get; set; }
        private string TemplateSuffix { get; set; }
        private string InterpolatedSuffix { get; set; }
        private CommandLineOptions GlobalOptions { get; set; }


        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async System.Threading.Tasks.Task<int> RunAsync(CancellationToken breaker)
        {

            var root = "";
            if (InputUriOption.Contains("://"))
            {
                var uri = new Uri(InputUriOption);
                root = uri.Scheme + "://" + Path.GetDirectoryName(uri.LocalPath);
            }
            else
            {
                root = Path.GetDirectoryName(InputUriOption);
            }

            _logger.LogInformation("Reading taskdef from {0}", InputUriOption);
            HashSet<string> _checked = new HashSet<string>();
            var taskdef = Google.Protobuf.JsonParser.Default.Parse<cz.ntx.proto.license.VersionedTasks>(new StreamReader(LazyStream.Input(InputUriOption)));
            foreach (var t in taskdef.Tasks)
            {
                foreach (var p in t.Profiles)
                {
                    var id = p.Id;
                    var uri = Path.Combine(root, id + TemplateSuffix);
                    var outputuri = Path.Combine(root, id + InterpolatedSuffix);
                    try
                    {
                        var text = new StreamReader(LazyStream.Input(uri)).ReadToEnd().ExpandEnvironment();
                        var task = Google.Protobuf.JsonParser.Default.Parse<cz.ntx.proto.scheduler.TaskConfiguration>(text);
                        p.Task = task;
                        var image = task.Image;
                        if (!_checked.Contains(image))
                        {
                            if (!task.ExistsOnDockerhub())
                            {
                                _logger.LogCritical("Image not exists: {0}", image);
                                throw new Exception("Image not exists");
                            }
                            _checked.Add(image);
                        }
                        if (!ReadOnly) {
                            _logger.LogInformation("Writing task to {0}", outputuri);
                            using (var w = new StreamWriter(LazyStream.Output(outputuri,"application/json"), new UTF8Encoding(false)))
                            {
                                await w.WriteLineAsync(text);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        _logger.LogCritical(ex.Message);
                        throw;
                    }

                }
            }
            _logger.LogInformation("Writing index to {0}", OutputUriOption);
            using (var w = new StreamWriter(LazyStream.Output(OutputUriOption, "application/json"), new UTF8Encoding(false)))
            {
                await w.WriteLineAsync(taskdef.ToString());
            }

            return 0;
        }


    }
}
