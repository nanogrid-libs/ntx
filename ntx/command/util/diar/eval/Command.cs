﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using System.IO;
using ntx.api.pipe.tasks;
using System.Threading.Tasks;
using cz.ntx.proto.v2t.misc;
using ntx.api;
using ntx.api.pipe.events;
using ntx.api.util;
using System.Text;
using System.Collections.Generic;
using Dasync.Collections;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace ntx.command.util.diar.eval
{
    class Command : ICommand
    {

        private static ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.command.util.diar.eval");
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "converts events to raw text";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes csv file: utterance_id|spk_id|voiceprint"
             + Environment.NewLine
             + "Produces: raw text"
             + Environment.NewLine;
            var inputUriOption = command.Option("-i|--input",
                "test file url",
                CommandOptionType.SingleValue
                );
            var enrollUriOption = command.Option("-e|--enroll",
              "enroll file url",
              CommandOptionType.SingleValue
              );
            var outputUriOption = command.Option("-o|--output <->",
                "output file url",
                CommandOptionType.SingleValue
                );
            var scoringOption = command.Option("-s|--scoring <average>",
              "scoring type average|deadmatch",
              CommandOptionType.SingleValue
              );
            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    EnrollUriOption = enrollUriOption.GetValueOrDefault(),
                    ScoringOption = scoringOption.GetValueOrDefault()
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string EnrollUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private string ScoringOption { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var inputResolver = LazyStream.Input(InputUriOption, breaker);
            var enrollResolver = LazyStream.Input(EnrollUriOption, breaker);
            var outputResolver = LazyStream.Output(OutputUriOption, "text/plain", breaker);
            var textSink = PipeSink.StringChunkStream(outputResolver);

            // read enrollment and prepare models
            _logger.LogInformation("Reading enrollment set");
            var enroll = (await VoiceprintUtils.CSVEnumerator(enrollResolver).ToListAsync());
            var nspeakers = enroll.Select(x => x.SpkId).Distinct().Count();
            _logger.LogInformation($"Read enrollment set with {enroll.Count} items and {nspeakers} speakers");

            _logger.LogInformation($"Preparing enrollment set for scoring type {ScoringOption}");
            if (ScoringOption == "average")
            {
                enroll = enroll.MergeBySpeakerId();
            }

            var test = VoiceprintUtils.CSVEnumerator(inputResolver);


            var correct_speakers = new List<Voiceprint.VoceprintResult>();
            var incorrect_speakers = new List<Voiceprint.VoceprintResult>();
            long hit = 0;
            long total = 0;
            _logger.LogInformation("Processing test set");
            await test.ForEachAsync(x =>
                {
                    var res = x.CompareWith(enroll);
                    if (res[0].VoicePrint1.SpkId == res[0].VoicePrint2.SpkId)
                    {
                        hit++;
                    }

                    correct_speakers.AddRange(res.FindAll(v => v.VoicePrint1.SpkId == v.VoicePrint2.SpkId));
                    incorrect_speakers.AddRange(res.FindAll(v => v.VoicePrint1.SpkId != v.VoicePrint2.SpkId));
                    total++;
                }
            );
            //find intersection
            correct_speakers = correct_speakers.OrderBy(x => x.LogLikelihood).ToList();
            incorrect_speakers = incorrect_speakers.OrderBy(x => x.LogLikelihood).ToList();
            //threshold for FRR = 0%
            var threshold_frr0 = correct_speakers.First().LogLikelihood;
            //threshold for FAR = 0%
            var threshold_far0 = incorrect_speakers.Last().LogLikelihood + 1e-6;

            var thresholds = incorrect_speakers.Select(x => x.LogLikelihood).Where(x => x >= threshold_frr0).OrderBy(x=>x).ToList();
            var incorrect_speakers_count = thresholds.Count();
            if (thresholds.Count() == 0)
            {
                thresholds.Add((float)(threshold_frr0 + threshold_far0) / 2.0f);
                incorrect_speakers_count = 0;
            }

            _logger.LogInformation("Estimating EER");
            //sample ROC - dummy approach with 1000 samples along FAR axis
            var nSamples = 1000;
            var step = thresholds.Count() < nSamples ? 1 : thresholds.Count() / nSamples;
            var ROC = new List < Voiceprint.ROCPoint>();
            
            foreach (var threshold in thresholds.Where((x, i) => i % step == 0))
            {
                var FAR = incorrect_speakers_count / (0.01f * incorrect_speakers.Count);
                var FRR = correct_speakers.Count(x => x.LogLikelihood < threshold) / (0.01f * correct_speakers.Count);
                incorrect_speakers_count -= step;
                ROC.Add(new Voiceprint.ROCPoint() { Threshold = threshold, FAR = FAR, FRR = FRR});
            }
            // get eer
            var eer_point = ROC.OrderBy(item => Math.Abs(item.FAR - item.FRR)).First();
            _logger.LogInformation("Estimation of EER completed");

            await textSink.WriteAsync(string.Format("N={3}\tS={4}\tCOR={0:0.00}\tEER={1:0.00}\tTHR={2}", 
                hit/(0.01f*total),0.5f *(eer_point.FAR + eer_point.FRR),eer_point.Threshold, total, nspeakers) + Environment.NewLine);
            await textSink.CompleteAsync();
            return 0;
        }


    }
}