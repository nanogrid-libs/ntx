﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using System.Threading.Tasks;
using ntx.api.pipe.events;
using Microsoft.Extensions.Logging;
using ntx.api;

namespace ntx.command.util.trsx.create
{
    class Command : ICommand
    {
       
        private static ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.command.trsx.create");
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "converts diar + transcription events to trsx format";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes: ntx.v2t.engine.Events from diar engine"
             + Environment.NewLine
             + "Produces: trsx format"
             + Environment.NewLine + Environment.NewLine;

             var inputUriOption = command.Option("-i|--input <none>",
                "input url of diarization events file",
                CommandOptionType.SingleValue
                );
            var transUriOption = command.Option("-t|--transcription <none>",
                "input url of diar events file",
                CommandOptionType.SingleValue
                );
            var outputUriOption = command.Option("-o|--output <->",
                "output url",
                CommandOptionType.SingleValue
                );
            var mediaUriOption = command.Option("-m|--media <none>",
                "media uri to link with",
                CommandOptionType.SingleValue
                );

            command.OnExecute(() =>
            {
                if (inputUriOption.GetValueOrDefault() == "none" && transUriOption.GetValueOrDefault() == "none")
                {
                    command.ShowHelp();
                    command.Error.WriteLine($"Please set  {inputUriOption.LongName} or {transUriOption.LongName}");
                    throw new Exception("Invalid option");
                }
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.GetValueOrDefault(),
                    TransUriOption = transUriOption.GetValueOrDefault(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    MediaUriOption = mediaUriOption.GetValueOrDefault()
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string TransUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private string MediaUriOption { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var outputResolver = LazyStream.Output(OutputUriOption, "text/plain", breaker);
            var textSink = PipeSink.StringChunkStream(outputResolver);
           

            Dasync.Collections.AsyncEnumerable<Event> transSource =
                TransUriOption == "none" ? null : PipeSource.FromProtoJsonStream<Events>(LazyStream.Input(TransUriOption, breaker), breaker)
                    .ViaLookAheadFilter()
                    .ToEventStream();

            var dummyDiarInput = PipeSource.FromList(new System.Collections.Generic.List<Event>() {
                new Event(){ Timestamp = new Event.Types.Timestamp(){ Timestamp_= 0}},
                new Event(){ Label = new Event.Types.Label(){ Speaker = "Unknown"}},
                new Event(){ Timestamp = new Event.Types.Timestamp(){ Timestamp_= (ulong)long.MaxValue}}
            });

            Dasync.Collections.AsyncEnumerable<Event> diarSource =
                InputUriOption == "none" ? dummyDiarInput :
                 PipeSource.FromProtoJsonStream<Events>(LazyStream.Input(InputUriOption, breaker), breaker)
                 .ViaLookAheadFilter()
                 .ToEventStream()
                    //TODO remove after update
                    .ViaInterceptor(
                      x =>
                      {
                          if (x.BodyCase == Event.BodyOneofCase.Label && x.Label.LabelCase == Event.Types.Label.LabelOneofCase.Item)
                          {
                              return new Event() { Label = new Event.Types.Label() { Speaker = x.Label.Item } };
                          }
                          return x;
                      }
                     
                     )
                 ;


            await diarSource.ToTRSXFormat(MediaUriOption, transSource).RunWithSink(textSink);
            
            

            return 0;
        }
    }
}