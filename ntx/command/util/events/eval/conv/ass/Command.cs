﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using System.IO;
using ntx.api.pipe.tasks;
using System.Threading.Tasks;
using cz.ntx.proto.v2t.misc;
using Dasync.Collections;
using ntx.api.pipe.events;
using System.Text;
using ntx.api.util;
using ConsoleTables;
using Microsoft.Extensions.Logging;
using ntx.api;

namespace ntx.command.util.events.eval.conv.ass
{
    class Command : ICommand
    {
        private static ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.command.util.events.eval.conv.html");
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "ass subtitle format";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes:  ntx.v2t.misc.Evaluation.Item"
             + Environment.NewLine
             + "Produces: ass"
             + Environment.NewLine + Environment.NewLine;

             var inputUriOption = command.Option("-i|--input",
                "input url",
                CommandOptionType.SingleValue
                );
            var outputUriOption = command.Option("-o|--output <->",
             "output url",
             CommandOptionType.SingleValue
             );

            var blockDuration = command.Option("--maxdur <4000>",
               "max insert block duration",
               CommandOptionType.SingleValue
               );
            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    BlockDuration = uint.Parse(blockDuration.GetValueOrDefault()),
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private uint BlockDuration { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {

            Evaluation.Types.Item content;
            using (var inputResolver = LazyStream.Input(InputUriOption, breaker))
            {
                using (StreamReader _stream = new StreamReader(inputResolver, new UTF8Encoding()))
                {
                    var line = await _stream.ReadToEndAsync();
                    content = Evaluation.Types.Item.Parser.ParseJson(line);
             
                }
            }
            using (var output = new StreamWriter(LazyStream.Output(OutputUriOption, "text/plain"), new UTF8Encoding(false)))
            {
                await content.WriteAsAss(output, TimeSpan.FromMilliseconds((int)BlockDuration));
            }
            return 0;
        }
    }
}