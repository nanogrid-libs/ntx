﻿using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using System.Threading.Tasks;

namespace ntx.command.util.events.eval.conv
{
    
    class Command :ICommand
    {
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Command("html", (c) => html.Command.Configure(c,options));
            command.Command("ass", (c) => ass.Command.Configure(c, options));
            command.Command("mine", (c) => mine.Command.Configure(c, options));
            command.Description = "convert evaluation to various formats";
            command.HelpOption("-h|--help");
            command.OnExecute(() =>
            {
                options.Command = new Command(command);

                return 0;
            });
            
        }
        private readonly CommandLineApplication _app;
        public Command(CommandLineApplication app)
        {
            _app = app;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            await _app.ShowHelpAsync();
            return 1;
        }


    }
}
