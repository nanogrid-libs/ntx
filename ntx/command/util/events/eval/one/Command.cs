﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;

using ntx.api.pipe.tasks;
using System.Threading.Tasks;
using cz.ntx.proto.v2t.misc;
using Dasync.Collections;
using ntx.api.pipe.events;
using ntx.api.util;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ntx.api;

namespace ntx.command.util.events.eval.one
{
    class Command : ICommand
    {

        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "evaluate v2t performance";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             Environment.NewLine
             + "Consumes: ntx.v2t.engine.Events"
             + Environment.NewLine
             + "Produces:  ntx.v2t.misc.Evaluation.Item"
             + Environment.NewLine
             + "Every line of reference file forms single eval block"
             + Environment.NewLine
             + Environment.NewLine
             + $"default split: {Constants.DefaultTextSplit}" + Environment.NewLine
             + $"default match: {Constants.DefaultPlusMatch}" + Environment.NewLine;

            var inputUriOption = command.Option("-i|--input",
                "input result url",
                CommandOptionType.SingleValue
                );
            var referenceUriOption = command.Option("-r|--reference <none>",
              "input reference url",
              CommandOptionType.SingleValue
              );
            var outputUriOption = command.Option("-o|--output <->",
                "output url",
                CommandOptionType.SingleValue
                );

            var blockDuration = command.Option("--maxdur <4500>",
               "max non-match block duration (in ms) inside matching block",
               CommandOptionType.SingleValue
               );
            var splitOption = command.Option($"-s|--split <default>",
                "split regex",
                CommandOptionType.SingleValue
                );

            var plusOption = command.Option(@"-p|--plus <default>",
                "match plus items",
                CommandOptionType.SingleValue
                );
            var idOption = command.Option(@"--id <default>",
                "set evaluation id",
                CommandOptionType.SingleValue
                );
            var removeTags = command.Option(@"--rmtags",
                "remove tags in input",
                CommandOptionType.NoValue
                );
            var nbest = command.Option("--nbest <1000>",
               "use nbest hypos in alignment",
               CommandOptionType.SingleValue
               );
            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    ReferenceOption = referenceUriOption.GetValueOrDefault(),
                    BlockDuration = uint.Parse(blockDuration.GetValueOrDefault()),
                    SplitOption = splitOption.GetValueOrDefault(),
                    PlusOption = plusOption.GetValueOrDefault(),
                    IdOption = idOption.GetValueOrDefault(),
                    RmTags= removeTags.HasValue(),
                    NBest = (int)uint.Parse(nbest.GetValueOrDefault())
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private string ReferenceOption { get; set; }
        private uint BlockDuration { get; set; }
        private string SplitOption { get; set; }
        private string PlusOption { get; set; }
        private string IdOption { get; set; }
        private bool RmTags { get; set; }
        private int NBest { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {

            var input= LazyStream.Input(InputUriOption,breaker);
            
            var output = LazyStream.Output(OutputUriOption, "application/json",breaker);

            Regex split = new Regex(SplitOption == "default" ? Constants.DefaultTextSplit : SplitOption);
            Regex plus = new Regex(PlusOption == "default" ? Constants.DefaultPlusMatch : PlusOption);

            ulong maxStopReal = ulong.MinValue;
            ulong maxStopStream = ulong.MinValue;

            var transform = @"<\S+?/>";
            if (!RmTags)
                transform = null;

            var eval = new Evaluation.Types.Item { Id = IdOption == "default" ? InputUriOption : IdOption, Speed =new Speed() };

            Event removeRecovery(Event x)
            {

                if (x.BodyCase == Event.BodyOneofCase.Timestamp &&
                 x.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Recovery
                )
                    return null;

                return x;
            }

            var resList = await PipeSource.FromProtoJsonStream<Events>(input, breaker)
                .ViaLookAheadFilter()
                .ViaEventFilter(removeRecovery)
                .ViaInterceptor(x =>
               {

                   if (x.ReceivedAt > maxStopReal)
                       maxStopReal = x.ReceivedAt;
                   return x;
               }
                )
                .ToEventStream()
                .ViaInterceptor(x =>
                {

                    if(x.BodyCase!= Event.BodyOneofCase.Timestamp )
                        return x;
                    
                    switch(x.Timestamp.ValueCase)
                    {
                        case Event.Types.Timestamp.ValueOneofCase.Timestamp_:
                            if (x.Timestamp.Timestamp_ > maxStopStream)
                                maxStopStream = x.Timestamp.Timestamp_;
                            break;
                        case Event.Types.Timestamp.ValueOneofCase.Recovery:
                            if (x.Timestamp.Recovery > maxStopStream)
                                maxStopStream = x.Timestamp.Recovery;
                            break;
                    }

                    return x;
                }
                )
                .ToEventsStream()
                .ViaReLabelAndSplit(split, plus)
                .ViaLabelMerge()
                .ToEventStream()
                .ViaLabelItemTransform(transform,"")
                .ToListAsync();

            if (maxStopReal != ulong.MinValue)
                eval.Speed.ProcessingDuration = maxStopReal;
            if (maxStopStream != ulong.MinValue)
                eval.Speed.StreamDuration = maxStopStream;

            List<AlignBlock> alignment=null;

            if (ReferenceOption == "none")
            {
                alignment = resList.ToDummyAlignment()
                    .FineTune(TimeSpan.FromMilliseconds(BlockDuration));
            }
            else
            {
                var reference = LazyStream.Input(ReferenceOption, breaker);
                var refBlockList = await PipeSource.EventsFromRawTextStream(reference, breaker)
                .ViaLookAheadFilter().ViaReLabelAndSplit(split, plus).ViaLabelMerge().ToListAsync();

                alignment = refBlockList.ToTextList("REF")
                .AlignWith(resList.ToTetxList("RES"),true,NBest)
                .ToAlignBlocks(refBlockList, resList)
                .FineTune(TimeSpan.FromMilliseconds(BlockDuration));
            }

            eval.Blocks.AddRange(alignment);
            eval.GetResults();
            var sink = PipeSink.ProtoJsonStream<Evaluation.Types.Item>(output);
            await sink.WriteAsync(eval);
            await sink.CompleteAsync();
            return 0;
        }
    }
}