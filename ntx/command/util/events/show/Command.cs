﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using System.IO;
using ntx.api.pipe.tasks;
using System.Threading.Tasks;
using cz.ntx.proto.v2t.misc;
using ntx.api;
using ntx.api.pipe.events;
using ntx.api.util;

namespace ntx.command.util.events.show
{
    class Command : ICommand
    {

        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "show events as raw text on console";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes: ntx.v2t.engine.Events"
             + Environment.NewLine
             + "Produces: raw text"
             + Environment.NewLine + Environment.NewLine
             + "Filters: " + Environment.NewLine
             + " noise:$timeSeconds:$symbolToProduce - nonspeech breaking filter" + Environment.NewLine;
            var inputUriOption = command.Option("-i|--input",
                "input url",
                CommandOptionType.SingleValue
                );
            var filterOption = command.Option(@"-f|--filter <noise:2.0:>",
                "filter to use",
                CommandOptionType.SingleValue
                );
         
            command.OnExecute(() =>
            {
                if (!inputUriOption.HasValue())
                {
                    command.Error.WriteLine($"Missing: {inputUriOption.Description}");
                    command.ShowHelp();
                    return 1;
                }
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    FilterOption = filterOption.GetValueOrDefault(),
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string FilterOption { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var inputResolver = LazyStream.Input(InputUriOption,breaker);
            var eventFilter = new EventFilter().ParseCmd(FilterOption);
            var consoleSink = PipeSink.ConsoleEvents();
            await PipeSource.FromProtoJsonStream<Events>(inputResolver, breaker).ViaFilterAsync(eventFilter).RunWithSink(consoleSink);
            return 0;
        }
    }
}