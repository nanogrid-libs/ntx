﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using Dasync.Collections;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using ntx.api.auth;
using ntx.api.pipe.events;
using ntx.api;
using ntx.api.pipe.tasks;
using Microsoft.Extensions.Logging;
using ntx.api.util;

namespace ntx.command.task.run.ntx.v2t.engine.EngineService
{
    class Command : ICommand
    {
        const string serviceId = "ntx.v2t.engine.EngineService";
        private static ILogger _logger = Logging.LoggerFactory.CreateLogger(serviceId);
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "streaming media transcription client";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText = 
                "Consumes: binary audio stream" 
                + Environment.NewLine 
                + "Produces: ntx.v2t.engine.Events";
            var inputUriOption = command.Option("-i|--input", 
                "input url", 
                CommandOptionType.SingleValue
                );
            
            var outputUriOption = command.Option("-o|--output <->",
                "output url", 
                CommandOptionType.SingleValue
                );
            var labelOption = command.Option("-l|--label",
                "task label => see: ntx task list",
                CommandOptionType.SingleValue
                );
            var tagsOption = command.Option(@"-t|--tags",
              "task tags => see:  ntx task list",
              CommandOptionType.MultipleValue
              );
            var audioFormatOption = command.Option("-a|--audioformat <auto:0>",
                "format auto:$probesizeBytes|pcm:$sampleFormat:$sampleRate:$channelLayout",
                CommandOptionType.SingleValue
                );
            var chunkSizeOption = command.Option($"-c|--chunkSize <4096>",
               "input chunk size in bytes",
               CommandOptionType.SingleValue
               );
            var lexiconOption = command.Option($"--lexicon <none>",
              "user lexicon uri",
              CommandOptionType.SingleValue
              );
            var parallelism = command.Option("-p|--parallelism <0>"
              , "use parallelism, one zero means streaming"
             , CommandOptionType.SingleValue);

            var parallNoMerge = command.Option("--nomerge"
               , "disable parallel block merging"
              , CommandOptionType.NoValue);
            var parallOverlap = command.Option("--overlap <5000>"
               , "set parallel block overlapping in ms"
              , CommandOptionType.SingleValue);
            var retryOption = command.Option("--retry <5:1000:1.5>"
               , "retry with exponencial backof count:initDelayMs:multiplier"
              , CommandOptionType.SingleValue);
            var channelOption = command.Option("--channel <downmix>"
               , "choose audio channel, dowmix|left|right"
              , CommandOptionType.SingleValue);
            var pushPullModel = command.Option("--fc"
               , "use push-pull model"
              , CommandOptionType.NoValue);

            command.OnExecute(() =>
            {
                if(!inputUriOption.HasValue())
                {
                    command.Error.WriteLine($"Missing: {inputUriOption.Description}");
                    command.ShowHelp();
                    return 1;
                }


                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    LabelOption = labelOption.GetValueOrDefault(),
                    TagsOption = tagsOption.HasValue() ? tagsOption.Values.ToArray() : null,
                    AudioFormatOption = audioFormatOption.GetValueOrDefault(),
                    ChunkSizeBytes = uint.Parse(chunkSizeOption.GetValueOrDefault()),
                    GlobalOptions = options,
                    LexiconUriOption = lexiconOption.GetValueOrDefault(),
                    Paralelism = int.Parse(parallelism.GetValueOrDefault()),
                    NoMerge = parallNoMerge.HasValue(),
                    Overlap = TimeSpan.FromMilliseconds(uint.Parse(parallOverlap.GetValueOrDefault())),
                    Retry = retryOption.GetValueOrDefault(),
                    ChannelOption = channelOption.GetValueOrDefault(),
                    PushPullModelOption = pushPullModel.HasValue(),
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private string LabelOption { get; set; }
        private string AudioFormatOption { get; set; }
        private string[] TagsOption { get; set; }
        private CommandLineOptions GlobalOptions { get; set; }
        private string LexiconUriOption { get; set; }
        private int Paralelism { get; set; }
        private bool NoMerge { get; set; }
        private TimeSpan Overlap { get; set; }
        private string Retry { get; set; }
        private string ChannelOption { get; set; }
        private uint ChunkSizeBytes { get; set; }
        private bool PushPullModelOption { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async System.Threading.Tasks.Task<int> RunAsync(CancellationToken breaker)
        {
            var retry = RetryWithBackoff.ParseFromCmd(Retry);

            


            //stream with valid token
            var tokenStream = LazyStream.Input(GlobalOptions.TaskAuthToken);
            //valid access token generator
            var accessTokenFactory = Authorization.AccesTokenFactory(tokenStream);

            //select task and authorize it 

            //cz.ntx.proto.license.Task task = new cz.ntx.proto.license.Task { Profiles =}

            cz.ntx.proto.license.Task task = null;
            //Use task selector to choose one - this option is depreceated
            try
            {
                task = await PipeSource.TasksFromLocalStore(accessTokenFactory, breaker)
                    .ViaFilterAsync(serviceId, LabelOption, TagsOption).FirstAsync().ViaInterceptor(
                        x =>
                        {
                            _logger.LogInformation($"Using task: {x.Service} -l {x.Profiles[0].Labels[0]} -t {string.Join(" -t ", x.Tags)}");
                            return x;
                        }
                    );
            }catch
            {
                if (TagsOption?.Length == 1 && LabelOption !=null)
                {
                    task = new cz.ntx.proto.license.Task { Profiles = { new cz.ntx.proto.license.Task.Types.Profile { Id = TagsOption[0], Labels = { LabelOption } } } };
                }
                else
                {
                    throw;
                }
            }
            
            var authorizedTaskFactory = task.ToAuthorizedTaskFactory(accessTokenFactory);
            //Print task info

            var inputStreamResolver = LazyStream.Input(InputUriOption,breaker);
            var outputResolver = LazyStream.Output(OutputUriOption, "application/json",breaker);

            
            //sevice Uri
            var endpointUri = new Uri((await accessTokenFactory()).ServiceEndpoint);
            //Create grpc channel, channel can be used for multiple clients of various services
            var channel = endpointUri.ToGRPCChannel();

            //Fill v2t config
            var engineContext = new EngineContext()
                .ParseCmd(LabelOption,ChannelOption);
            engineContext.AudioFormat = new AudioFormat()
                .ParseCmd(AudioFormatOption); ;

            //Add lexicon if any
            await engineContext.AddLexiconFromUri(LexiconUriOption);
            //Create client on channel
            var client = new cz.ntx.proto.v2t.engine.EngineService.EngineServiceClient(channel);
            if (InputUriOption == "empty://dog2p")
            {
                //Create sink from output stream
                var lexsink = PipeSink.ProtoJsonStream<Lexicon>(
                     outputResolver
                    );
                await engineContext.ViaG2PStreaming(client, authorizedTaskFactory).RunWithSink(lexsink);
                return 0;
            }

            //Select reader
            bool textInput = engineContext.ConfigCase == EngineContext.ConfigOneofCase.Ppc;
            bool isParallel = textInput ? false : Paralelism > 0;
           
           
            if (isParallel && engineContext.AudioFormat.FormatsCase != AudioFormat.FormatsOneofCase.Pcm)
                throw new Exception("Only raw pcm input is supported!");
           
            //Create pipe source
            var eventsSource = textInput ? 
                PipeSource.FromProtoJsonStream<Events>(inputStreamResolver,breaker)
                .ViaEventsLimiterByCount(10)
                : PipeSource.EventsFromBinaryStream(inputStreamResolver,  (int)ChunkSizeBytes, breaker);
            
            


            //Create sink from output stream
            var sink = PipeSink.ProtoJsonStream<Events>(
                 outputResolver
                );

            if (isParallel)
            {
                //run a very very simple parallel v2t
                await eventsSource.ViaV2TParallel(client, Paralelism, engineContext, NoMerge, Overlap, retry, authorizedTaskFactory, breaker, PushPullModelOption).RunWithSink(sink);
            }
            else
            {
                //Run pipe
                await eventsSource.ViaV2TStreaming(client, engineContext, authorizedTaskFactory, breaker, PushPullModelOption).RunWithSink(sink);
            }
            return 0;
        }


    }
}
