﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using System.IO;
using ntx.api.io;
using ntx.api.pipe;
using ntx.api.pipe.tasks;
using Dasync.Collections;
using System.Threading.Tasks;

namespace ntx.command.task.run
{
    class Command : ICommand
    {
        private string LabelOption { get; set; }
        private string[] TagsOption { get; set; }
        private CommandLineOptions GlobalOptions { get; set; }
        
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "run task on remote cluster";
            command.HelpOption("-h|--help");
            command.Command("ntx.v2t.engine.EngineService", (c) => ntx.v2t.engine.EngineService.Command.Configure(c, options));
            command.OnExecute(() =>
            {
                options.Command = new Command(command);
                return 0;
            });
        }

        private readonly CommandLineApplication _app;
        public Command(CommandLineApplication app)
        {
            _app = app;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            await _app.ShowHelpAsync();
            return 0;
        }
    }
}
