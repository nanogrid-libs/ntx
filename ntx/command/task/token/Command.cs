﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using Dasync.Collections;
using ntx.api.pipe;
using ntx.api.auth;
using ntx.api;
using ntx.api.pipe.tasks;
using Microsoft.Extensions.Logging;

namespace ntx.command.task.token
{
    class Command : ICommand
    {
        private static ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.command.task.token");
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
           
            command.Description = "get auth token for task";
            command.HelpOption("-h|--help");
            var outputUriOption = command.Option("-o|--output <->",
                "output url",
                CommandOptionType.SingleValue
                );
            var labelOption = command.Option("-l|--label",
                "task label => see: ntx task list",
                CommandOptionType.SingleValue
                );
            var tagsOption = command.Option(@"-t|--tags",
              "task tags => see:  ntx task list",
              CommandOptionType.MultipleValue
              );
            var serviceOption = command.Option(@"-s|--service <>",
              "task tags => see:  ntx task list",
              CommandOptionType.SingleValue
              );
            command.OnExecute(() =>
            {
                options.Command = new Command(command)
                {

                    ServiceOption = serviceOption.GetValueOrDefault(),
                    LabelOption = labelOption.GetValueOrDefault(),
                    TagsOption = tagsOption.HasValue() ? tagsOption.Values.ToArray() : null,
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    GlobalOptions = options
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string OutputUriOption { get; set; }
        private string ServiceOption { get; set; }
        private string LabelOption { get; set; }
        private string[] TagsOption { get; set; }
        private CommandLineOptions GlobalOptions { get; set; }


        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async System.Threading.Tasks.Task<int> RunAsync(CancellationToken breaker)
        {
            
            var tokenStream = LazyStream.Input(GlobalOptions.TaskAuthToken);
            var accessTokenFactory = Authorization.AccesTokenFactory(tokenStream);
            //Use task selector to choose one
            var task = await PipeSource.TasksFromLocalStore(accessTokenFactory, breaker)
                .ViaFilterAsync(ServiceOption, LabelOption, TagsOption).FirstAsync().ViaInterceptor(
                    x =>
                    {
                        _logger.LogInformation($"Using task: {x.Service} -l {x.Profiles[0].Labels[0]} -t {string.Join(" -t ", x.Tags)}");
                        return x;
                    }
                );
            var authorizedTaskFactory = task.ToAuthorizedTaskFactory(accessTokenFactory);

            var outputStreamResolver = LazyStream.Output(OutputUriOption,"text/plain");
            var sink = PipeSink.StringChunkStream(outputStreamResolver);
            await sink.WriteAsync((await authorizedTaskFactory()).Token_);
            return 0;
        }


    }
}
