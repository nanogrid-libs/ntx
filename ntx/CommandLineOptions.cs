﻿using Microsoft.Extensions.CommandLineUtils;
using ntx.command;
using Serilog.Events;
using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ntx
{
    public static class Helper
    {
        public async static Task ShowHelpAsync(this CommandLineApplication app)
        {
            await Task.Run(() => app.ShowHelp());
        }

        public static string GetValueOrDefault(this CommandOption option)
        {
            return option.HasValue() ? option.Value() : option.ValueName;
        }

        public static void MustSetValue(this CommandOption option,CommandLineApplication command)
        {
            if(!option.HasValue())
            {
                command.ShowHelp();
                command.Error.WriteLine($"Please set  --{option.LongName}"+ Environment.NewLine+Environment.NewLine);
                throw new Exception("Invalid option");
            }
        }
    }
    
    public class CommandLineOptions
    {
        public string LogLevel { get; set; }
        public ICommand Command { get; set; }
        public string Version { get; set; }
        public string TaskAuthToken { get; set; }
        public string LogFilter { get; set; }
        public string EnvFile { get; set; }
        public long MaxRunTime { get; set; }
        private static string[] SplitAsCmdArguments(string args)
        {
            char[] parmChars = args.ToCharArray();
            bool inSingleQuote = false;
            bool inDoubleQuote = false;
            bool escaped = false;
            bool lastSplitted = false;
            bool justSplitted = false;
            bool lastQuoted = false;
            bool justQuoted = false;

            int i, j;

            for (i = 0, j = 0; i < parmChars.Length; i++, j++)
            {
                parmChars[j] = parmChars[i];

                if (!escaped)
                {
                    if (parmChars[i] == '^')
                    {
                        escaped = true;
                        j--;
                    }
                    else if (parmChars[i] == '"' && !inSingleQuote)
                    {
                        inDoubleQuote = !inDoubleQuote;
                        parmChars[j] = '\n';
                        justSplitted = true;
                        justQuoted = true;
                    }
                    else if (parmChars[i] == '\'' && !inDoubleQuote)
                    {
                        inSingleQuote = !inSingleQuote;
                        parmChars[j] = '\n';
                        justSplitted = true;
                        justQuoted = true;
                    }
                    else if (!inSingleQuote && !inDoubleQuote && parmChars[i] == ' ')
                    {
                        parmChars[j] = '\n';
                        justSplitted = true;
                    }

                    if (justSplitted && lastSplitted && (!lastQuoted || !justQuoted))
                        j--;

                    lastSplitted = justSplitted;
                    justSplitted = false;

                    lastQuoted = justQuoted;
                    justQuoted = false;
                }
                else
                {
                    escaped = false;
                }
            }

            if (lastQuoted)
                j--;

            return (new string(parmChars, 0, j)).Split(new[] { '\n' });
        }

        public static CommandLineOptions Parse(string cmd)
        {
            return Parse(SplitAsCmdArguments(cmd));
        }
        public static CommandLineOptions Parse(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            
            
            var options = new CommandLineOptions();

            var app = new CommandLineApplication
            {
                Name = "ntx",
                FullName = "ntx cloud .net cli"
                 
            };

            //Get program version
            options.Version = typeof(CommandLineOptions)
            .GetTypeInfo()
            .Assembly
            .GetCustomAttribute<AssemblyInformationalVersionAttribute>()
            .InformationalVersion;
            
            app.HelpOption("-h|--help");
            
            var loggingOption = app.Option("-l|--loglevel <info>", 
                "global logging level trace|debug|info|warn|error", 
                CommandOptionType.SingleValue);
            var loggingFilterOption = app.Option("-f|--logfilter <.*>",
                "global logging filter",
                CommandOptionType.SingleValue);
            var taskAuthEndPoint = app.Option("-t|--token <env://NTX_TOKEN>"
                ,"authorization token"
                ,CommandOptionType.SingleValue);
            var envFilePath = app.Option("-e|--env <.env>"
                , "default path with environment"
                , CommandOptionType.SingleValue);

            var maxRunTime = app.Option("-r|--runtime <0>"
                , "max running time in seconds"
                , CommandOptionType.SingleValue);

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                if (Environment.GetEnvironmentVariable("HOME") == null)
                {
                    Environment.SetEnvironmentVariable("HOME", 
                        Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%"
                        ));
                }
            }
            string tokenPath = Path.Combine(
                    Environment.GetEnvironmentVariable("HOME"), ".ntx", "access_token");

            command.Command.Configure(app,options);
            
            try
            {
                var result = app.Execute(args);
                if(result!=0)
                    throw new Exception("Parsing failed");
            }catch
            {
                return null;
            }
            options.LogLevel = loggingOption.GetValueOrDefault();
            options.TaskAuthToken = taskAuthEndPoint.GetValueOrDefault();
            options.LogFilter = loggingFilterOption.GetValueOrDefault();
            options.EnvFile = envFilePath.GetValueOrDefault();
            options.MaxRunTime = (long)ulong.Parse(maxRunTime.GetValueOrDefault());

            return options;
        }
        public static LogEventLevel ParseLoggingLevel(string cmd)
        {
            switch (cmd)
            {
                case "trace":
                    return LogEventLevel.Verbose;
                case "debug":
                    return LogEventLevel.Debug;
                case "info":
                    return LogEventLevel.Information;
                case "warn":
                    return LogEventLevel.Warning;
                case "error":
                    return LogEventLevel.Error;
                default:
                    throw new Exception($"invalid loglevel {cmd}");
            }
        }

    }
}
