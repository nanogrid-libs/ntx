#!/bin/bash
cp -r . /build
cd /build/ntx
perl /work/scripts/make_version ${PROJECT_VERSION} ntx.csproj
dotnet restore
# dotnet publish -r win10-x64 -c release -o /work/release/win10-x64
# dotnet publish -r osx.10.11-x64 -c release -o /work/release/osx.10.11-x64
dotnet publish -c release -o /work/release/ntx
cd /work
cp /work/scripts/ntx.bat /work/release/ntx
cp /work/scripts/ntx.sh /work/release/ntx/ntx
chmod 777 /work/release/ntx/ntx
