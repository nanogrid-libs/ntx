#!/bin/bash
cp -r . /build
cd /build/ntx
perl /work/scripts/make_version ${PROJECT_VERSION} ntx.csproj
dotnet restore
dotnet publish -r ${RID} -c release -o /work/release/${RID}
