PROJECT_GIT_HASH := $(shell git rev-parse --short HEAD)

COMPANY_NAME := nanotrix
PROJECT_NAME := ntx

PROJECT_VERSION := $(shell git rev-parse --abbrev-ref HEAD)


IMAGE_NAME := nanotrix/$(PROJECT_NAME):$(PROJECT_VERSION)
NET_BUILD_IMAGE ?= mcr.microsoft.com/dotnet/sdk:5.0-focal

PROTOS_DIR=$(CURDIR)/../protobuf
NET_RID ?=osx-x64 #win-x64;linux-x64

.PHONY: build
build: clean
	@echo Building project in $(NET_BUILD_IMAGE)
	@docker run --rm -v $(CURDIR):/work -w /work -e PROJECT_VERSION=$(PROJECT_VERSION)-$(PROJECT_GIT_HASH) $(NET_BUILD_IMAGE) bash scripts/build.sh

.PHONY: binaries
binaries: clean
	@echo Building binaries $(NET_RID) in $(NET_BUILD_IMAGE)
	@docker run --rm -v $(CURDIR):/work -w /work -e PROJECT_VERSION=$(PROJECT_VERSION)-$(PROJECT_GIT_HASH) -e RID=$(NET_RID) $(NET_BUILD_IMAGE) bash scripts/build-binary.sh

.PHONY: image
image: build
	@echo Building image $(IMAGE_NAME)
	@docker build -t $(IMAGE_NAME)-latest -t $(IMAGE_NAME)-$(PROJECT_GIT_HASH) .

.PHONY: protos
protos: 
	@echo Updating protobuf 
	 $(eval PROTO_VERSION := $(shell git -C $(PROTOS_DIR) rev-parse --abbrev-ref HEAD)-$(shell git -C $(PROTOS_DIR) rev-parse HEAD))
	@docker run --rm -v $(CURDIR):/work -v $(PROTOS_DIR):/protos -w /work -e PROTO_VERSION=$(PROTO_VERSION) $(NET_BUILD_IMAGE) bash scripts/protos.sh
	
.PHONY: image-push
image-push: pre-push-check image
	@echo Pushing $(IMAGE_NAME)-latest
	@docker push $(IMAGE_NAME)-latest
	@docker push $(IMAGE_NAME)-$(PROJECT_GIT_HASH)

.PHONY: clean
clean: 
	@docker run --rm -v $(CURDIR):/work -w /work $(NET_BUILD_IMAGE) rm -rf release

.PHONY: test
test: 
	@echo Testing project in $(NET_BUILD_IMAGE)
	@docker run --rm -v $(CURDIR):/work -w /work $(NET_BUILD_IMAGE) bash scripts/test.sh

.PHONY: pre-push-check
pre-push-check:
	@echo Checking for git status
	@git diff-files --quiet

.PHONY: publish
publish: pre-push-check build zip
	@echo publishing
	@mc cp $(CURDIR)/release/$(PROJECT_NAME)-$(PROJECT_VERSION)-latest.zip wasabi/apps.nanotrix.cloud/install/

.PHONY: zip
zip:
	@cd release && zip -r $(PROJECT_NAME)-$(PROJECT_VERSION)-$(PROJECT_GIT_HASH).zip ntx && zip -r $(PROJECT_NAME)-$(PROJECT_VERSION)-latest.zip ntx