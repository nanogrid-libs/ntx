# Building 
Configure git (on windows only - before clone):
* `git config --global core.autocrlf=input`
* `git config --global core.eol = lf` 

## With docker
1. Get [docker](https://www.docker.com/)
3. If on Windows install [GNU Make](http://gnuwin32.sourceforge.net/packages/make.htm) and add it to your PATH
4. type `make`
5. binaries should appear in release folder 

## With dotnet
1. Install [.net core sdk 5.0.x](https://www.microsoft.com/net/download/core)
2. `cd ntx`
2. `dotnet restore`
3. `dotnet publish -c release -o ../release/dotnet`

## With VS2019
1. Install [VS2019+](https://www.visualstudio.com/downloads/)
2. open solution and build

# Referencing 
1. Add ntx.api/ntx.api.csproj project to your solution
2. Add it to references
7. See ntx project for example usage
3. For version control, use e.g. [git-vendor](https://github.com/brettlangdon/git-vendor), just copy /bin/git-vedor script to your git path to make it working.

# Code structure
Code directly follows cli commands, e.g. implementation of command:
1. `ntx task list` => ntx/commad/task/list/Command.cs
2. `ntx task run ntx.v2t.engine.EngineService` => ntx/commad/task/run/ntx.v2t.engine.EngineService/Command.cs
3. folder `ntx.api/protos` contains automatically generated protobuf messages and [GRPC](http://www.grpc.io/) client code
4. All nanotrix GRPC services + proto definitions: [https://gitlab.com/nanotrix/protobuf](https://gitlab.com/nanotrix/protobuf)
5. V2T ntx.v2t.engine.EngineService and all i/o messages definitions are in file: `ntx/v2t/engine/engine.proto`

# Download
1. Install [.net core runtime 5.0.x](https://www.microsoft.com/net/download/core#/runtime)
2. Download the latest [ntx](https://s3.eu-central-1.wasabisys.com/apps.nanotrix.cloud/install/ntx-1.5.2-latest.zip) version
3. Unpack to any folder
4. Add it to your system path
    
# CLI Examples
1. Get token:
    * ask cluster admin for acount
    * copy+paste temporary access token from cluster endpoint ui
    * or get permanent login token: `ntx auth login -u username -a  https://xxx.nanogrid.cloud -r`
1. Set access/refresh token: 
    * on windows: `setx NTX_TOKEN $TOKEN`
    * on unix: `export NTX_TOKEN=$TOKEN`
    * or put it to file and use `ntx --token path_to_your_token` in all commands
    * or put it to .env file NTX_TOKEN=$TOKEN in working directory
    * or change .env file path, see help `ntx --help`
    * test `ntx task list` to see list of available tasks
2. Revoke token: 
    * acces token only: `ntx auth logout`
2. Get current login info:
    * `ntx auth info`
2. Get help:
    * `ntx` returns options and list of subcommands, choose one, e.g. task
    * `ntx task`, choose one, e.g. list
    * `ntx task list --help`
3. List avalailable tasks from store:
    * `ntx task list`
    * filter by tags: `ntx task list -t lang:sl -t type:broadcast`
4. Run task:
    * `ntx task run $taskParams` - to get $taskParams just copy one line from `ntx task list`
    * get help for slovenian broadcast vad+v2t+ppc: 
    ```
    ntx task run ntx.v2t.engine.EngineService -l vad+v2t+ppc -t lang:sl -t type:broadcast
    ```
    * set input multimedia stream or file or process (e.g. for live recording or codec conversion)
        * stdin: `-i -`
        * file: `-i path_to_file`
        * url: `-i http://icecast8.play.cz/cro1-128.mp3`
        * process stdout: `-i "proc://ffmpeg -nostats -re -f dshow -i audio=\"Mikrofon (C-Media USB Headphone Set  )\" -ac 1 -ar 16000 -f s16le -"`
    * set input audio format:
        * `-a auto:0` (default) means automatic format/codec detection, for some mp3 streams, probe size in bytes must be specified, e.g. `-a auto:8192` 
        * `-a pcm:s16le:16000:mono` - for raw pcm data 16000Hz sample rate, single channel
        * for multiple audio streams formats, the one with the highest bitrate is chosen
    * select audio channel: --channel option can change the default behaviour, i.e. downmixing to single channel
        * Options:
            * downmix (default) - create mono 
            * left - convert to stereo and use left channel
            * right - convert to stereo and use right channel
    * set output:
        * stdout: `-o -` (default): prints to stdout in json format, one Events message per line
        * file: `-o xxx.json`
5. Utils
    * `ntx util events cat`: filter Event messages and prints raw text
    * `ntx util events show`: filter Event messages and prints raw text with step-back to simulate live dictation, works on windows only, .net core and console on linux/osx are not best friends

## Transcribe radio live:
```
ntx task run ntx.v2t.engine.EngineService -l vad+v2t+ppc -t lang:cz -t mode:transcribe -i http://icecast8.play.cz/cro1-128.mp3 -a auto:8192 | ntx util events cat -i -
```

## Dictate on windows via ffmpeg capture: 
```
ntx task run ntx.v2t.engine.EngineService -l v2t+ppc -t lang:cz -t mode:dictate -i "proc://ffmpeg -nostats -re -f dshow -i audio=\"Mikrofon (C-Media USB Headphone Set  )\" -ac 1 -ar 16000 -f s16le -blocksize 640 -" -a pcm:s16le:16000:mono | ntx util events show -i -
```

## Transcribe parallel:
```
ntx task run ntx.v2t.engine.EngineService -l vad+v2t+ppc -t lang:cz -t type:broadcast -i "proc://ffmpeg -i files/basetext.mp3 -ac 1 -ar 16000 -f s16le -" -a  pcm:s16le:16000:mono -p 2
```

## Keyword spotting live:
```
ntx task run ntx.v2t.engine.EngineService -l v2t -t lang:cz -t mode:control --lexicon files/userlex-radiozurnal.json -i http://icecast8.play.cz/cro1-128.mp3 -a auto:8192 | ntx util events log -i - --live
```

To get list of devices call:
`ffmpeg -list_devices true -f dshow -i dummy`

## Evaluate v2t results: 
* Do alignment of v2t result with reference file
    ```
    ntx util events eval one -i files/alignment/longaudio.res.json -r files/alignment/longaudio.txt -o files/alignment/longaudio.eval.json
    ```
* Print result summary (use multiple inputs -i to make summary of multiple files)
    ```
    ntx util events eval summary -i files/alignment/longaudio.eval.json
    ```
* Print results in html 
    ```
    ntx util events eval conv html -i files/alignment/longaudio.eval.json -o files/alignment/longaudio.eval.html
    ```

