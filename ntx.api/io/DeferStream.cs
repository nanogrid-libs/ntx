﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace ntx.api.io
{
    public class DeferStream<T> : Stream
    {
        Stream _stream;
        Action<T> _defer;
        T _context;
        public DeferStream(Stream stream, T context, Action<T> defer) 
        {
            _stream = stream;
            _defer = defer;
            _context = context;
        }

        public override bool CanRead => _stream.CanRead;

        public override bool CanSeek => _stream.CanSeek;

        public override bool CanWrite => _stream.CanWrite;

        public override long Length => _stream.Length;

        public override long Position { get => _stream.Position; set => _stream.Position = value; }

        public override void Flush()
        {
            _stream.Flush();
        }
        public string ContentType { private set; get; }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _stream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _stream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _stream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _stream.Write(buffer, offset, count);
        }
        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                _defer(_context);
            }
            base.Dispose(disposing);
        }
    }
}
