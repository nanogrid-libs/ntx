﻿using cz.ntx.proto.v2t.misc;
using ntx.api.pipe.events;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ntx.api.util
{
    public static class HtmlDump
    {

        private static async Task WriteAsHtml(this AlignBlock block, StreamWriter writer,bool withNoises)
        {
            await writer.WriteLineAsync($"<BR>Type: {block.BType}");
            if (block.Position != null)
            {
                await writer.WriteLineAsync($"<BR>Offset: {TimeSpan.FromTicks((long)block.Position.Offset)}");
                await writer.WriteLineAsync($"<BR>Duration: {TimeSpan.FromTicks((long)block.Position.Duration)}");
            }
            if (block.Score != null)
            {
                await writer.WriteLineAsync($"<BR>Score: {string.Format(CultureInfo.InvariantCulture, "{0:0.00} ({1:0.00}) [H={2}, D={3}, S={4}, I={5}, N={6}]", 100 * block.Score.Correctness, 100 * block.Score.Accuracy, block.Score.Hits, block.Score.Deletions, block.Score.Substitutions, block.Score.Insertions, block.Score.Count)}");
            }
            if(block.Dscore != null)
            {
                await writer.WriteLineAsync("<BR>Diar: " + string.Format(CultureInfo.InvariantCulture, "R = {0:0.00} P = {1:0.00} F = {2:0.00} C= {3:0.00} [H={4}, D={5}, I={6}, N={7}, M={8}, C={9}]",
                    100 * block.Dscore.Recall,
                    100 * block.Dscore.Precission,
                    100 * block.Dscore.Frate,
                    100 * block.Dscore.ClusterPurity,
                    block.Dscore.Hits, block.Dscore.Deletions, block.Dscore.Insertions, block.Dscore.Count, block.Dscore.ClusterMatch, block.Dscore.ClusterCount));
            }
            string table1 = "<TD align=\"left\"><B>REF:</B></TD>"; ;
            string table2 = "<TD align=\"left\"><B>RES:</B></TD>"; ;
            foreach (var a in block.Alignment)
            {
                switch (a.AlignmentCase)
                {
                    case Alignment.AlignmentOneofCase.AlignedLabel:

                        switch (a.AlignedLabel.AType)
                        {
                            case Alignment.Types.AlignType.None:
                                break;
                            case Alignment.Types.AlignType.Hit:
                                table1 += "<TD align=\"left\"><B>" + WebUtility.HtmlEncode(a.AlignedLabel.Ref.GetValue()) + "</B></TD>";
                                table2 += "<TD align=\"left\"><B>" + WebUtility.HtmlEncode(a.AlignedLabel.Res.GetValue()) + "</B></TD>";
                                break;
                            case Alignment.Types.AlignType.Sub:
                                table1 += "<TD align=\"left\"><B><B><FONT COLOR=\"RED\">" + WebUtility.HtmlEncode(a.AlignedLabel.Ref.GetValue()) + "</FONT></B></TD>";
                                table2 += "<TD align=\"left\"><B><FONT COLOR=\"RED\">" + WebUtility.HtmlEncode(a.AlignedLabel.Res.GetValue()) + "</FONT></B></TD>";
                                break;
                            case Alignment.Types.AlignType.Ins:

                                if (a.AlignedLabel.Res.LabelCase == cz.ntx.proto.v2t.engine.Event.Types.Label.LabelOneofCase.Noise)
                                {
                                    var print = withNoises ? a.AlignedLabel.Res.GetValue() : "";

                                    table1 += "<TD align=\"left\"><B><B><FONT COLOR=\"BLUE\">" + "" + "</FONT></B></TD>";
                                    table2 += "<TD align=\"left\"><B><FONT COLOR=\"BLUE\">" + print + "</FONT></B></TD>";
                                }
                                else
                                {
                                    table1 += "<TD align=\"left\"><B><B><FONT COLOR=\"BLUE\">" + " " + "</FONT></B></TD>";
                                    table2 += "<TD align=\"left\"><B><FONT COLOR=\"BLUE\">" + WebUtility.HtmlEncode(a.AlignedLabel.Res.GetValue()) + "</FONT></B></TD>";
                                }
                                break;
                            case Alignment.Types.AlignType.Del:
                                table1 += "<TD align=\"left\"><B><B><FONT COLOR=\"BLUE\">" + WebUtility.HtmlEncode(a.AlignedLabel.Ref.GetValue()) + "</FONT></B></TD>";
                                table2 += "<TD align=\"left\"><B><FONT COLOR=\"BLUE\">" + " " + "</FONT></B></TD>";
                                break;
                        }

                        break;
                    case Alignment.AlignmentOneofCase.AlignedTimeStamp:
                        if (a.AlignedTimeStamp.Timestamp.ValueCase == cz.ntx.proto.v2t.engine.Event.Types.Timestamp.ValueOneofCase.Timestamp_)
                        {
                            table1 += string.Format("<TD title=\"{0}\"><B><B><FONT COLOR=\"GREEN\">" + "|" + "</FONT></B></TD>", TimeSpan.FromTicks((Int64)a.AlignedTimeStamp.Timestamp.Timestamp_));
                            table2 += string.Format("<TD title=\"{0}\"><B><B><FONT COLOR=\"GREEN\">" + "|" + "</FONT></B></TD>", TimeSpan.FromTicks((Int64)a.AlignedTimeStamp.Timestamp.Timestamp_));
                        }
                        break;
                    case Alignment.AlignmentOneofCase.AlignedSpeakerTurn:
                        switch (a.AlignedSpeakerTurn.AType)
                        {
                            case Alignment.Types.AlignType.None:
                                break;
                            case Alignment.Types.AlignType.Hit:
                                table1 += "<TD title=\"" + WebUtility.HtmlEncode(a.AlignedSpeakerTurn.Ref) + "\"><B><FONT COLOR=\"BLACK\">&#x1F5E3;</FONT></B></TD>";
                                table2 += "<TD title=\"" + WebUtility.HtmlEncode(a.AlignedSpeakerTurn.Res ) + "\"><B><FONT COLOR=\"BLACK\">&#x1F5E3;</FONT></B></TD>";
                                break;
                            case Alignment.Types.AlignType.Sub:
                                table1 += "<TD title=\"" + WebUtility.HtmlEncode(a.AlignedSpeakerTurn.Ref) + "\"><B><FONT COLOR=\"RED\">&#x1F5E3;</FONT></B></TD>";
                                table2 += "<TD title=\"" + WebUtility.HtmlEncode(a.AlignedSpeakerTurn.Res ) + "\"><B><FONT COLOR=\"RED\">&#x1F5E3;</FONT></B></TD>";
                                break;
                            case Alignment.Types.AlignType.Ins:
                                table1 += "<TD align=\"left\"><B><FONT COLOR=\"BLUE\">" + " " + "</FONT></B></TD>";
                                table2 += "<TD title=\"" + WebUtility.HtmlEncode(a.AlignedSpeakerTurn.Res) + "\"><B><FONT COLOR=\"BLUE\">&#x1F5E3;</FONT></B></TD>";
                                break;
                            case Alignment.Types.AlignType.Del:
                                table1 += "<TD title=\"" + WebUtility.HtmlEncode(a.AlignedSpeakerTurn.Ref) + "\"><B><FONT COLOR=\"BLUE\">&#x1F5E3;</FONT></B></TD>";
                                table2 += "<TD align=\"left\"><B><FONT COLOR=\"BLUE\">" + " " + "</FONT></B></TD>";
                                break;
                        }
                        break;
                }
            }
            await writer.WriteLineAsync("<TABLE BORDER=0>");
            if (block.Alignment.Count > 0)
            {
                await writer.WriteLineAsync($"<TR>{table1}");
                await writer.WriteLineAsync($"<TR>{table2}");
            }
            await writer.WriteLineAsync("</TABLE>");
        }
        public static async Task WriteAsHtml(this Evaluation.Types.Item eval, StreamWriter writer,bool withNoises)
        {
            await writer.WriteLineAsync($"<HTML><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><TITLE>ASR results {eval.Id}</TITLE>");

            await writer.WriteLineAsync($"<BR>ID: {eval.Id}<BR>");
            if (eval.Speed != null)
            {
                await writer.WriteLineAsync($"<BR>Speed: {String.Format("{0:0.00}", eval.Speed.Factor)}");
                await writer.WriteLineAsync($"<BR>Duration: {TimeSpan.FromTicks((long)eval.Speed.StreamDuration)}");
            }
            await writer.WriteLineAsync("<BR>Score: " + string.Format(CultureInfo.InvariantCulture, "{0:0.00} ({1:0.00}) [H={2}, D={3}, S={4}, I={5}, N={6}]", 100 * eval.Score.Correctness, 100 * eval.Score.Accuracy, eval.Score.Hits, eval.Score.Deletions, eval.Score.Substitutions, eval.Score.Insertions, eval.Score.Count));
            if (eval.Dscore != null)
            {
                await writer.WriteLineAsync("<BR>Diar: " + string.Format(CultureInfo.InvariantCulture, "R = {0:0.00} P = {1:0.00} F = {2:0.00} C= {3:0.00} [H={4}, D={5}, I={6}, N={7}, M={8}, C={9}]", 
                    100 * eval.Dscore.Recall, 
                    100 * eval.Dscore.Precission, 
                    100 * eval.Dscore.Frate, 
                    100 * eval.Dscore.ClusterPurity,
                    eval.Dscore.Hits, eval.Dscore.Deletions,eval.Dscore.Insertions, eval.Dscore.Count,eval.Dscore.ClusterMatch,eval.Dscore.ClusterCount));

                if(eval.Dscore.SpeakerMap != null)
                {
                    var spk1 = "<TD align=\"left\"><B>REF:</B></TD>";
                    var spk2 = "<TD align=\"left\"><B>RES:</B></TD>";
                    foreach (var s in eval.Dscore.SpeakerMap)
                    {
                        spk1 += $"<TD align=\"center\"><B>{s.Ref}</B></TD>";
                        spk2 += $"<TD align=\"center\"><B>{s.Res}</B></TD>";
                    }
                    await writer.WriteLineAsync("<TABLE BORDER=1>");
                    await writer.WriteLineAsync($"<TR>{spk1}");
                    await writer.WriteLineAsync($"<TR>{spk2}");
                    await writer.WriteLineAsync("</TABLE>");
                }
            }

            await writer.WriteLineAsync("<HR>");
            foreach (var block in eval.Blocks)
            {
                await block.WriteAsHtml(writer, withNoises);
                await writer.WriteLineAsync("<HR>");
            }
            await writer.WriteLineAsync("</HTML>");
            await writer.FlushAsync();
            
        }
        
    }
}
