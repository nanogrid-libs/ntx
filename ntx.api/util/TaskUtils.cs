﻿using ntx.api.io;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ntx.api.util
{
    public static class TaskUtils
    {
        public static bool ExistsOnDockerhub(this cz.ntx.proto.scheduler.TaskConfiguration task)
        {

            try
            {
                var root = "https://index.docker.io/v1/repositories/";
                if(task.Image.Contains("://"))
                {
                    root = "";
                }
                var path = root + task.Image.Replace(":", "/tags/");
                using (var r = new StreamReader(LazyStream.Input(path)))
                {
                    var resp = r.ReadToEnd();
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

       

    }
}
