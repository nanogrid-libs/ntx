﻿using ICSharpCode.SharpZipLib.Tar;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ntx.api.util
{

    public class QuillDeltaContents
    {
        public class Ops
        {
            [JsonProperty("insert")]
            public string Insert { get; set; }
        }
        [JsonProperty("ops")]
        public Ops[] Operations { get; set; }

        public static async Task<QuillDeltaContents> FromStream(Stream stream)
        {
            return JsonConvert.DeserializeObject<QuillDeltaContents>(await new StreamReader(stream).ReadToEndAsync());
        }


        public Stream ToRawTextStream()
        {
            using (var streamWriter = new StreamWriter(new MemoryStream(), Encoding.UTF8, 4096, true))
            {
                foreach (var o in Operations)
                {
                    foreach (var v in o.Insert.Split(new string[] { "\n" }, StringSplitOptions.None))
                    {
                        streamWriter.WriteLine(v);
                    }

                }
                streamWriter.Flush();
                streamWriter.BaseStream.Seek(0, SeekOrigin.Begin);
                return streamWriter.BaseStream;
            }
        }
    }

    public static class NtaHelper
    {

        public static Dictionary<string, Stream> FetchStreamsFromTar(Stream inputStream, string select)
        {
            Dictionary<string, Stream> list = new Dictionary<string, Stream>();
            TarInputStream tarIn = new TarInputStream(inputStream);
            TarEntry tarEntry;
            while ((tarEntry = tarIn.GetNextEntry()) != null)
            {
                if (Regex.IsMatch(tarEntry.Name,select))
                {
                    var m = new MemoryStream();
                    tarIn.CopyEntryContents(m);
                    m.Seek(0, SeekOrigin.Begin);
                    list[tarEntry.Name] = m;
                }
            }

            return list;

        }
    }
}
