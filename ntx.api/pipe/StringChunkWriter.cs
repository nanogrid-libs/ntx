﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace ntx.api.pipe
{
    public class StringChunkWriter : IAsyncSink<string>
    {
        StreamWriter stream = null;
        Stream streamResolver;
        
        public StringChunkWriter(Stream streamResolver)
        {
            this.streamResolver = streamResolver;
        }
        public async  Task WriteAsync(string item)
        {
            if (stream == null)
            {
                stream = new StreamWriter(streamResolver, new UTF8Encoding(false));
            }

            await stream.WriteAsync(item);
            await stream.FlushAsync();
        }
        public Task CompleteAsync()
        {
            return Task.CompletedTask;
        }
    }
}
