﻿using cz.ntx.proto.v2t.engine;
using Microsoft.Extensions.Logging;
using System;
using Dasync.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ntx.api.pipe.events
{
    public static partial class Extensions
    {
        public static Dasync.Collections.AsyncEnumerable<Lexicon> ViaG2PStreaming(this EngineContext context, EngineService.EngineServiceClient client,
              Func<Task<cz.ntx.proto.auth.NtxTokenResponse.Types.Token>> authResolver = null, CancellationToken breaker = default(CancellationToken))
        {
            ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.v2t.engine.EngineService");
            var start = DateTime.UtcNow;
            var cs = new CancellationTokenSource();
            var registration = breaker.Register(() => cs.Cancel());
            _logger.LogInformation("Allocating resources");
            return new AsyncEnumerable<Lexicon>(async yield => {
                cz.ntx.proto.auth.NtxTokenResponse.Types.Token authToken = null;
                if (authResolver != null)
                {
                    try
                    {
                        authToken = await authResolver();

                        var t = authToken.Token_.Split('.');
                        if (t.Length == 3)
                            _logger.LogInformation($"Task instance: {t[1]}");
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Task authorization failed: {ex.Message}");
                        throw ex;
                    }
                }
                using (var call = client.StreamingRecognize(Pipe.CreateDefaultGrpcCallOptions(cs.Token, authToken)))
                {



                    await call.RequestStream.WriteAsync(new EngineStream() { Start = new EngineContextStart() { Context = context } });
                    await call.ResponseStream.MoveNext(breaker);
                    var meta = await call.ResponseHeadersAsync;
                    foreach (var v in meta)
                    {
                        if (v.IsBinary)
                            continue;
                        _logger.LogInformation($"Task meta, {v.Key} = {v.Value}");
                    }
                    registration.Dispose();
                    var resp = call.ResponseStream.Current;

                    if (resp.PayloadCase != EngineStream.PayloadOneofCase.Start)
                        throw new Exception("start expected");
                    _logger.LogInformation("Task started, start time {0} s", (DateTime.UtcNow - start));
                    if (resp.Start.Context != null
                        && resp.Start.Context.ConfigCase == EngineContext.ConfigOneofCase.V2T
                        && resp.Start.Context.V2T.WithLexicon != null)
                    {
                        await yield.ReturnAsync(resp.Start.Context.V2T.WithLexicon);
                    }
                    _logger.LogTrace("\t>>>PULL");
                    await call.RequestStream.WriteAsync(new EngineStream() { Pull = new EventsPull() });
                    await call.ResponseStream.MoveNext(breaker);
                    var response = call.ResponseStream.Current;
                    if (response.PayloadCase != EngineStream.PayloadOneofCase.Pull)
                        throw new Exception("Pull expected");

                    await call.RequestStream.WriteAsync(new EngineStream() { End = new EngineContextEnd() });
                    await call.ResponseStream.MoveNext(breaker);
                    response = call.ResponseStream.Current;
                    if(response.PayloadCase != EngineStream.PayloadOneofCase.End)
                        throw new Exception("End expected");
                    _logger.LogTrace("Completing ...");
                    await call.RequestStream.CompleteAsync();
                    var ret = await call.ResponseStream.MoveNext(breaker);
                    _logger.LogTrace("Completing done with {0}", ret);
                    _logger.LogInformation("Completed");
                }
            });
        }




        public static Dasync.Collections.AsyncEnumerable<Events> ViaV2TStreaming(this Dasync.Collections.AsyncEnumerable<Events> audioSource, EngineService.EngineServiceClient client,
             EngineContext context, Func<Task<cz.ntx.proto.auth.NtxTokenResponse.Types.Token>> authResolver = null, CancellationToken breaker = default(CancellationToken), bool EnablePushPull = false){
            ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.v2t.engine.EngineService");
            var start = DateTime.UtcNow;
            var cs = new CancellationTokenSource();
            var registration=breaker.Register(() => cs.Cancel());
            _logger.LogInformation("Allocating resources");
            if (!EnablePushPull)
            {
                return new AsyncEnumerable<Events>(async yield => {
                    cz.ntx.proto.auth.NtxTokenResponse.Types.Token authToken = null;
                    if (authResolver != null)
                    {
                        try
                        {
                            authToken = await authResolver();

                            var t = authToken.Token_.Split('.');
                            if (t.Length == 3)
                                _logger.LogInformation($"Task instance: {t[1]}");
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError($"Task authorization failed: {ex.Message}");
                            throw ex;
                        }
                    }
                   using (var call = client.StreamingRecognize(Pipe.CreateDefaultGrpcCallOptions(cs.Token, authToken,!EnablePushPull)))
                   {



                       await call.RequestStream.WriteAsync(new EngineStream() { Start = new EngineContextStart() { Context = context } });
                       await call.ResponseStream.MoveNext(breaker);
                       var meta = await call.ResponseHeadersAsync;
                       foreach (var v in meta)
                       {
                           if (v.IsBinary)
                               continue;
                           _logger.LogInformation($"Task meta, {v.Key} = {v.Value}");
                       }
                       registration.Dispose();
                       var resp = call.ResponseStream.Current;

                       if (resp.PayloadCase != EngineStream.PayloadOneofCase.Start)
                           throw new Exception("start expected");
                       _logger.LogInformation("Task started, start time {0} s", (DateTime.UtcNow - start));
                       if (resp.Start.Context != null
                           && resp.Start.Context.ConfigCase == EngineContext.ConfigOneofCase.V2T
                           && resp.Start.Context.V2T.WithLexicon != null)
                       {
                           _logger.LogInformation("Using lexicon: {0}",
                           System.Convert.ToBase64String(
                               System.Text.Encoding.UTF8.GetBytes(
                                   resp.Start.Context.V2T.WithLexicon.ToString())));
                       }
                       var source = audioSource.GetAsyncEnumerator();

                       var startTime = DateTime.UtcNow;


                       var upstream = Task.Run(async () =>
                          {
                              while (true)
                              {
                                  if (await source.MoveNextAsync())
                                  {
                                      _logger.LogTrace("\t>>>PUSH");
                                      await call.RequestStream.WriteAsync(new EngineStream() { Push = new EventsPush() { Events = source.Current } });

                                  }
                                  else
                                  {
                                      _logger.LogTrace("\t>>>END");
                                      _logger.LogDebug("Writing END message");
                                      await call.RequestStream.WriteAsync(new EngineStream() { End = new EngineContextEnd() });
                                      _logger.LogDebug("Closing upstream");
                                      await call.RequestStream.CompleteAsync();
                                      break;

                                  }
                              }
                              
                          }
                       );
                       bool done = false;
                       while (true)
                       {
                           if(!await call.ResponseStream.MoveNext(breaker))
                            {
                                _logger.LogDebug("Closed downstream");
                                break;
                            }
                           var response = call.ResponseStream.Current;
                           switch (response.PayloadCase)
                           {
                               case EngineStream.PayloadOneofCase.End:
                                   _logger.LogTrace("<<<END");
                                    _logger.LogDebug("Read END message");
                                    done = true;
                                   break;
                               case EngineStream.PayloadOneofCase.Push:
                                   {
                                       _logger.LogTrace("<<<PUSH");
                                       response.Push.Events.ReceivedAt = (UInt64)DateTime.UtcNow.Subtract(startTime).Ticks;
                                       await yield.ReturnAsync(response.Push.Events);
                                   }
                                   break;
                               default:
                                   throw new Exception($"Bad message: ${response}");
                           }
                       }

                       await upstream;
                       
                       //var ret = await call.ResponseStream.MoveNext(breaker);
                       _logger.LogDebug("Completing done with {0}", done);
                       _logger.LogInformation("Completed");
                   }
                });
            }
            else
            {
                _logger.LogInformation($"Using push-pull model");
                return new AsyncEnumerable<Events>(async yield => {
                    cz.ntx.proto.auth.NtxTokenResponse.Types.Token authToken = null;
                    if (authResolver != null)
                    {
                        try
                        {
                            authToken = await authResolver();

                            var t = authToken.Token_.Split('.');
                            if (t.Length == 3)
                                _logger.LogInformation($"Task instance: {t[1]}");
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError($"Task authorization failed: {ex.Message}");
                            throw ex;
                        }
                    }
                    using (var call = client.StreamingRecognize(Pipe.CreateDefaultGrpcCallOptions(cs.Token, authToken)))
                    {



                        await call.RequestStream.WriteAsync(new EngineStream() { Start = new EngineContextStart() { Context = context } });
                        await call.ResponseStream.MoveNext(breaker);
                        var meta = await call.ResponseHeadersAsync;
                        foreach (var v in meta)
                        {
                            if (v.IsBinary)
                                continue;
                            _logger.LogInformation($"Task meta, {v.Key} = {v.Value}");
                        }
                        registration.Dispose();
                        var resp = call.ResponseStream.Current;

                        if (resp.PayloadCase != EngineStream.PayloadOneofCase.Start)
                            throw new Exception("start expected");
                        _logger.LogInformation("Task started, start time {0} s", (DateTime.UtcNow - start));
                        if (resp.Start.Context != null
                            && resp.Start.Context.ConfigCase == EngineContext.ConfigOneofCase.V2T
                            && resp.Start.Context.V2T.WithLexicon != null)
                        {
                            _logger.LogInformation("Using lexicon: {0}",
                            System.Convert.ToBase64String(
                                System.Text.Encoding.UTF8.GetBytes(
                                    resp.Start.Context.V2T.WithLexicon.ToString())));
                        }
                        var source = audioSource.GetAsyncEnumerator();
                        var startTime = DateTime.UtcNow;
                        bool done = false;
                        while (!done)
                        {
                            bool pulled = false;
                            _logger.LogTrace(">>>PULL");
                            await call.RequestStream.WriteAsync(new EngineStream() { Pull = new EventsPull() });

                            while (!pulled)
                            {
                                await call.ResponseStream.MoveNext(breaker);
                                var response = call.ResponseStream.Current;
                                switch (response.PayloadCase)
                                {
                                    case EngineStream.PayloadOneofCase.End:
                                        _logger.LogTrace("<<<END");
                                        done = true;
                                        pulled = true;
                                        break;
                                    case EngineStream.PayloadOneofCase.Push:
                                        {
                                            _logger.LogTrace("<<<PUSH");
                                            response.Push.Events.ReceivedAt = (UInt64)DateTime.UtcNow.Subtract(startTime).Ticks;
                                            await yield.ReturnAsync(response.Push.Events);
                                            pulled = true;
                                        }
                                        break;
                                    case EngineStream.PayloadOneofCase.Pull:
                                        _logger.LogTrace("\t<<<PULL");
                                        if (await source.MoveNextAsync())
                                        {
                                            _logger.LogTrace("\t>>>PUSH");
                                            await call.RequestStream.WriteAsync(new EngineStream() { Push = new EventsPush() { Events = source.Current } });

                                        }
                                        else
                                        {
                                            _logger.LogTrace("\t>>>END");
                                            await call.RequestStream.WriteAsync(new EngineStream() { End = new EngineContextEnd() });

                                        }
                                        break;
                                }
                            }
                        }
                        _logger.LogTrace("Completing ...");
                        await call.RequestStream.CompleteAsync();
                        var ret = await call.ResponseStream.MoveNext(breaker);
                        _logger.LogTrace("Completing done with {0}", ret);
                        _logger.LogInformation("Completed");
                    }
                });
            }
        }
    }
}
