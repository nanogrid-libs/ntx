﻿using cz.ntx.proto.v2t.engine;
using Dasync.Collections;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace ntx.api.pipe.events
{
    public static partial class Extensions
    {
        public static Dasync.Collections.AsyncEnumerable<string> ToTRSXFormat(this Dasync.Collections.AsyncEnumerable<Event> eventsSource, string mediaUri)
        {
            return new AsyncEnumerable<string>(async yield =>
            {
                var source = eventsSource.GetAsyncEnumerator();
                TimeSpan lastTime = TimeSpan.Zero;
                long lastChp = -1;
                string cLabel = null;
                var root = new XElement("transcription",
                     new XAttribute("mediauri", mediaUri),
                     new XAttribute("version", "3.0")
                     );
                var channel = new XElement("se", new XAttribute("name", "transcription"));
                root.Add(new XElement("ch", new XAttribute("name", mediaUri),channel));
                root.Add(new XElement("sp"));
                while (await source.MoveNextAsync())
                {
                    var _event = source.Current;
                    switch (_event.BodyCase)
                    {
                        case Event.BodyOneofCase.Timestamp:
                            if(_event.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Timestamp_)
                            {
                                var cchp = (long)_event.Timestamp.Timestamp_; 
                                if (lastChp > -1)
                                {

                                    channel.Add(
                                            new XElement("pa",
                                                new XAttribute("a", ""),
                                                new XAttribute("b", TimeSpan.FromTicks(lastChp)),
                                                new XAttribute("e", TimeSpan.FromTicks(cchp)),
                                                new XAttribute("s", "-1"),
                                                new XElement("p",
                                                    new XAttribute("b", TimeSpan.FromTicks(lastChp)),
                                                    new XAttribute("e", TimeSpan.FromTicks(cchp)),
                                                    cLabel
                                                )
                                            ));
                                }
                                lastChp = cchp;
                            }
                            break;
                        case Event.BodyOneofCase.Label:
                            if (_event.Label.LabelCase == Event.Types.Label.LabelOneofCase.Speaker)
                            {
                                cLabel = _event.Label.Speaker;
                            }
                            break;
                    }
                }
                var doc = new XDocument(root) { Declaration = new XDeclaration("1.0", "UTF-8", "yes") };
                await yield.ReturnAsync(doc.Declaration.ToString() + "\n");
                await yield.ReturnAsync(doc.ToString());
            });
        }

        private static XElement GetBlock(this List<Event> events, long start, long stop, string cLabel, string id)
        {

            var ts = events.Where(x => x.BodyCase == Event.BodyOneofCase.Timestamp
            && x.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Timestamp_)
            .Select(x => (long)x.Timestamp.Timestamp_);

            var _start = ts
            .OrderBy(n => Math.Abs(n - start))
            .First();

            var _stop = ts
            .OrderBy(n => Math.Abs(n - stop))
            .First();
            var startIndex = events.FindIndex(
                x => x.BodyCase == Event.BodyOneofCase.Timestamp
                && x.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Timestamp_
                && x.Timestamp.Timestamp_ == (ulong)_start
                );
            var stopIndex = events.FindIndex(
                x => x.BodyCase == Event.BodyOneofCase.Timestamp
                && x.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Timestamp_
                && x.Timestamp.Timestamp_ == (ulong)_stop
                );

            List<XElement> words = new List<XElement>();
            long ws = (long)events[startIndex].Timestamp.Timestamp_;
            long we = ws;
            var w = "";
            var f = "";
            bool sb = true;
            var text = "";
            for (var i = startIndex + 1; i <= stopIndex; i++)
            {
                if (events[i].BodyCase == Event.BodyOneofCase.Timestamp
                    && events[i].Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Timestamp_)
                {
                    we = (long)events[i].Timestamp.Timestamp_;
                    words.Add(
                    new XElement("p",
                    new XAttribute("b", TimeSpan.FromTicks(ws)),
                    new XAttribute("e", TimeSpan.FromTicks(we)),
                    new XAttribute("f", f),
                    w));
                    text += w;
                    ws = we;
                    w = "";
                    f = "";
                    
                    continue;
                }
                if (events[i].BodyCase == Event.BodyOneofCase.Label
                    && (events[i].Label.LabelCase == Event.Types.Label.LabelOneofCase.Item ||
                    events[i].Label.LabelCase == Event.Types.Label.LabelOneofCase.Plus)
                    )
                {
                    var ev = events[i].Label.GetValue();
                    if (sb) {
                        if (ev.Trim().Length == 0)
                            continue;
                        else
                        {
                            sb = false;
                        }
                            
                    }
                    w += ev;
                    
                    continue;
                }

                if (events[i].BodyCase == Event.BodyOneofCase.Label
                    && (events[i].Label.LabelCase == Event.Types.Label.LabelOneofCase.Noise)
                    )
                {
                    f += "*";
                    continue;
                }


            }


            start = _start;
            stop = _stop;
            if (text.Length == 0)
                return null;

               return new XElement("pa",
                                               new XAttribute("a", ""),
                                               new XAttribute("b", TimeSpan.FromTicks(start)),
                                               new XAttribute("e", TimeSpan.FromTicks(stop)),
                                               new XAttribute("s", id),
                                               words

                                           );
        }

        public static Dasync.Collections.AsyncEnumerable<string> ToTRSXFormat(this Dasync.Collections.AsyncEnumerable<Event> diarSource, string mediaUri, Dasync.Collections.AsyncEnumerable<Event> transSource)
        {
            if (transSource == null)
                return diarSource.ToTRSXFormat(mediaUri);
            return new AsyncEnumerable<string>(async yield =>
            {
                
                var trans = await transSource.ToListAsync();
                var diar = diarSource.GetAsyncEnumerator();
                var channel = new XElement("se", new XAttribute("name", "transcription"));
                var speaker = new XElement("sp");
                Dictionary<string, string> speakers = new Dictionary<string, string>();
                TimeSpan lastTime = TimeSpan.Zero;
                long lastChp = -1;
                string cLabel = null;
                while (await diar.MoveNextAsync())
                {
                    var _event = diar.Current;
                    switch (_event.BodyCase)
                    {
                        case Event.BodyOneofCase.Timestamp:
                            if (_event.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Timestamp_)
                            {
                                var cchp = (long)_event.Timestamp.Timestamp_;
                                if (lastChp > -1)
                                {
                                    var content = trans.GetBlock(lastChp, cchp, cLabel, speakers[cLabel]);
                                    if(content!=null)
                                        channel.Add(content);
                                }
                                lastChp = cchp;
                            }
                            break;
                        case Event.BodyOneofCase.Label:
                            if (_event.Label.LabelCase == Event.Types.Label.LabelOneofCase.Speaker)
                            {
                                cLabel = _event.Label.Speaker;
                                if (!speakers.ContainsKey(cLabel))
                                {
                                    speakers[cLabel] = speakers.Count().ToString();
                                    speaker.Add(
                                        new XElement("s",
                                            new XAttribute("id", speakers[cLabel]),
                                            new XAttribute("surname", cLabel),
                                            new XAttribute("firstname", ""),
                                            new XAttribute("lang",""),
                                            new XAttribute("sex", "")
                                            )
                                       );
                                }
                            }
                            break;
                    }
                }




                var root = new XElement("transcription",
                    new XAttribute("mediauri", mediaUri),
                    new XAttribute("version", "3.0")
                    );
                root.Add(new XElement("ch", new XAttribute("name", mediaUri),channel));
                root.Add(speaker);
                var doc = new XDocument(root) { Declaration = new XDeclaration("1.0", "UTF-8", "yes") };
                await yield.ReturnAsync(doc.Declaration.ToString() + "\n");
                await yield.ReturnAsync(doc.ToString());
            });
        }
    }
}
