﻿using cz.ntx.proto.v2t.engine;
using Microsoft.Extensions.Logging;
using ntx.api.util;
using System;
using Dasync.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ntx.api.pipe.events
{
    public static partial class Extensions
    {


        static ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.v2t.engine.EngineService.ViaV2TParallel");
        internal static Dasync.Collections.AsyncEnumerable<Events> ViaV2TParallelNoMerge(this Dasync.Collections.AsyncEnumerable<Events> audioSource, EngineService.EngineServiceClient client, int parallelism,
             EngineContext context, TimeSpan overlap, RetryWithBackoff retry, Func<Task<cz.ntx.proto.auth.NtxTokenResponse.Types.Token>> authResolver = null, CancellationToken breaker = default(CancellationToken),
             bool EnablePushPull = false
             )
        {

            var bitRate = context.AudioFormat.Pcm.GetBitRate();
            var sampleRate = context.AudioFormat.Pcm.GetSampleRate();


            return new AsyncEnumerable<Events>(async yield =>
            {

                var sourceData = await audioSource.ViaAudioFetchLogger(bitRate).ToListAsync();
                var totalBytes = sourceData.GetAudioBytesCount();
                var totalDuration = (totalBytes * 80000000) / bitRate;
                var totalSamples = (totalDuration * sampleRate) / 10000000;
                _logger.LogInformation($"Duration: {TimeSpan.FromTicks(totalDuration)}");
                _logger.LogInformation($"Bytes: {totalBytes}");

                var maxPar = (int)(totalDuration / (overlap.Ticks * 10));
                if (maxPar < 1)
                    maxPar = 1;
                if (parallelism > maxPar)
                {
                    parallelism = maxPar;
                }
                _logger.LogInformation($"Parallelism: {parallelism}");
                var events = new List<Events>[parallelism];
                for (int i = 0; i < parallelism; i++)
                    events[i] = new List<Events>();

                long blockSamples = (totalSamples / parallelism);
                long blockDuration = (10000000 * blockSamples) / sampleRate;

                long overlapSamples = (overlap.Ticks * sampleRate) / 10000000;
                long overlapDuration = (10000000 * overlapSamples) / sampleRate;


                long totalOverlapedDuration = parallelism * blockDuration + (parallelism - 1) * overlapDuration;



                long processed = 0;
                long lastPrinted = 0;
                //approx.
                Action<long> ShowProgress = (x) =>
                {
                    var sum = Interlocked.Add(ref processed, x);
                    var pc = String.Format("{0:0.00}", (100.0 * sum) / (double)(totalOverlapedDuration));
                    if (processed - Interlocked.Read(ref lastPrinted) > 100000000)
                    {
                        _logger.LogInformation($"Progress: {pc}%");
                        Interlocked.Exchange(ref lastPrinted, sum);
                    }

                };
                var startTime = DateTime.UtcNow;

                //a very primitive strategy no sync, no problems, some v2t errors possible
                await events.ParallelForEachAsync(
                    async (e, id) =>
                    {


                        long from = id * blockDuration;
                        long to = (id + 1) * blockDuration + overlapDuration;
                        if (to > totalDuration)
                            to = totalDuration;
                        long recovery = from;
                        long bytesTo = ((bitRate * to) / 80000000);

                        long maxPos = recovery;

                        var _retry = retry.Clone();
                        _logger.LogDebug($"Block: {id}, From: {TimeSpan.FromTicks(from)}, To: {TimeSpan.FromTicks(to)}");

                        while (true)
                        {
                            try
                            {
                                long bytesFrom = ((bitRate * recovery) / 80000000);
                                _logger.LogDebug($"Block: {id}, FromBytes: {bytesFrom}, To: {bytesTo}");
                                var pipe = PipeSource.FromList(sourceData, breaker)
                                .ViaByteFilter(bytesFrom, bytesTo - bytesFrom)
                                .ViaV2TStreaming(client, context.Clone(), authResolver, breaker, EnablePushPull)
                                .ViaLookAheadFilter().WithOffset(recovery).ToEventStream();
                                e.Add(new Events());
                                await pipe.ForEachAsync(x =>
                                {
                                    retry.Reset();

                                    e.Last().Events_.Add(x);
                                    e.Last().ReceivedAt = (ulong)(DateTime.UtcNow - startTime).Ticks;
                                    if (x.BodyCase == Event.BodyOneofCase.Timestamp &&
                                        x.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Recovery
                                        )
                                    {
                                        recovery = (long)x.Timestamp.GetValue();
                                        if (recovery > maxPos)
                                        {
                                            var diff = recovery - maxPos;
                                            ShowProgress(diff);
                                            maxPos = recovery;
                                        }
                                    }

                                });
                                break;
                            }
                            catch (Exception ex)
                            {
                                _logger.LogWarning($"Failed: {ex.Message}");
                                if (breaker.IsCancellationRequested)
                                    throw (ex);
                                if (await _retry.Next(breaker))
                                    throw ex;
                                recovery = recovery - overlap.Ticks;
                                if (recovery <= from)
                                {
                                    recovery = from;
                                    e.Remove(e.Last());
                                }
                                _logger.LogWarning($"Retry #{_retry.Retry} of {_retry.MaxRetries} from: {TimeSpan.FromTicks(recovery)}");

                            }
                        }
                    },
                    parallelism
                    );

                foreach (var e in events)
                {
                    foreach (var ee in e)
                    {
                        await yield.ReturnAsync(ee);
                    }
                }
                if (!breaker.IsCancellationRequested)
                    _logger.LogInformation($"Progress: 100.00%");
                else
                    _logger.LogInformation($"Canceled");
            });
        }



        public static Dasync.Collections.AsyncEnumerable<Events> ViaV2TParallel(this Dasync.Collections.AsyncEnumerable<Events> audioSource, EngineService.EngineServiceClient client, int parallelism,
             EngineContext context, bool noMerge, TimeSpan overlap, RetryWithBackoff retry, Func<Task<cz.ntx.proto.auth.NtxTokenResponse.Types.Token>> authResolver = null, CancellationToken breaker = default(CancellationToken),
             bool EnablePushPull = false)
        {


            var ret = audioSource.ViaV2TParallelNoMerge(client, parallelism, context, overlap, retry, authResolver, breaker, EnablePushPull);
            if (!noMerge)
            {
                ret = ret.ViaV2TParallelBlockMerge();
            }
            return ret;
        }

        public static long GetAudioBytesCount(this List<Events> audioEvents)
        {
            long total = 0;
            foreach (var ev in audioEvents)
            {
                foreach (var e in ev.Events_)
                {
                    if (e.BodyCase != Event.BodyOneofCase.Audio)
                        continue;
                    total += e.Audio.Body.Length;
                }
            }
            return total;
        }


        public static Dasync.Collections.AsyncEnumerable<Events> ViaV2TParallelBlockMerge(this Dasync.Collections.AsyncEnumerable<Events> eventSource, CancellationToken breaker = default(CancellationToken))
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = eventSource.GetAsyncEnumerator();
                Events prevBlock = null;
                while (await source.MoveNextAsync())
                {
                    var ev = source.Current;

                    if (prevBlock == null)
                    {
                        prevBlock = ev;
                        continue;
                    }
                    var prevEndTs = prevBlock.GetLastTimeStamp();
                    var nextStartTs = ev.GetFirstTimeStamp();


                    if (prevEndTs.HasValue && nextStartTs.HasValue && nextStartTs.Value < prevEndTs.Value)
                    {
                        _logger.LogDebug($"Overlap detected: {TimeSpan.FromTicks((long)(prevEndTs.Value - nextStartTs.Value))}");
                        var pos = ev.GetLastTimeStamp(((prevEndTs.Value + nextStartTs.Value)) / 2);
                        //var prevOverLap= prevBlock.FilterByTimeStamp(nextStartTs-1, prevEndTs);
                        //var nextOverLap = ev.FilterByTimeStamp(nextStartTs-1, prevEndTs);
                        var ret = prevBlock.FilterByTimeStamp(null, pos);
                        await yield.ReturnAsync(ret);
                        prevBlock = ev.FilterByTimeStamp(pos, null);
                        continue;
                    }
                    await yield.ReturnAsync(prevBlock);
                    prevBlock = ev;


                }
                if (prevBlock != null)
                    await yield.ReturnAsync(prevBlock);

            });
        }
        internal static Events FilterByTimeStamp(this Events events, ulong? start, ulong? end)
        {
            var src = events.Events_.ToList();

            if (start.HasValue)
            {
                int pos = src.FindLastIndex(x => x.BodyCase == Event.BodyOneofCase.Timestamp &&
                    x.Timestamp.GetValue() <= start.Value
                );

                src = src.Skip(pos + 1).ToList();
            }

            if (end.HasValue)
            {
                int pos = src.FindLastIndex(x => x.BodyCase == Event.BodyOneofCase.Timestamp &&
                    x.Timestamp.GetValue() <= end.Value
                );

                src = src.Take(pos + 1).ToList();
            }

            var ret = events.Clone();
            ret.Events_.Clear();
            ret.Events_.Add(src);
            return ret;
        }

        internal static ulong? GetFirstTimeStamp(this Events events, ulong? biggerThan = null)
        {
            if (!biggerThan.HasValue)
                biggerThan = ulong.MinValue;

            var rts = events.Events_.FirstOrDefault(x => x.BodyCase == Event.BodyOneofCase.Timestamp
                      && x.Timestamp.GetValue() >= biggerThan
                      );
            if (rts != null)
                return rts.Timestamp.GetValue();

            return null;
        }

        internal static ulong? GetLastTimeStamp(this Events events, ulong? smallerThan = null)
        {
            if (!smallerThan.HasValue)
                smallerThan = ulong.MaxValue;
            var rts = events.Events_.LastOrDefault(x => x.BodyCase == Event.BodyOneofCase.Timestamp
                      && x.Timestamp.GetValue() < smallerThan
                      );
            if (rts != null)
                return rts.Timestamp.GetValue();
            return null;
        }
        internal static Dasync.Collections.AsyncEnumerable<Events> WithOffset(this Dasync.Collections.AsyncEnumerable<Events> eventSource, long offset, CancellationToken breaker = default(CancellationToken))
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = eventSource.GetAsyncEnumerator();
                while (await source.MoveNextAsync())
                {
                    var ev = source.Current;
                    foreach (var e in ev.Events_)
                    {
                        if (e.BodyCase != Event.BodyOneofCase.Timestamp)
                            continue;
                        long val = (long)e.Timestamp.GetValue() + offset;
                        if (val < 0)
                            val = 0;

                        switch (e.Timestamp.ValueCase)
                        {
                            case Event.Types.Timestamp.ValueOneofCase.Recovery:
                                e.Timestamp.Recovery = (ulong)val;
                                break;
                            case Event.Types.Timestamp.ValueOneofCase.Timestamp_:
                                e.Timestamp.Timestamp_ = (ulong)val;
                                break;
                        }

                    }
                    await yield.ReturnAsync(ev);
                }
            });
        }

        public static Dasync.Collections.AsyncEnumerable<Events> ViaByteFilter(this Dasync.Collections.AsyncEnumerable<Events> audioSource, long bytesFrom, long count, CancellationToken breaker = default(CancellationToken))
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = audioSource.GetAsyncEnumerator();
                long bytesRead = 0;
                long bytesLeaving = count;
                while (await source.MoveNextAsync())
                {
                    var ev = source.Current;
                    var ret = new Events();
                    foreach (var e in ev.Events_)
                    {
                        if (e.BodyCase != Event.BodyOneofCase.Audio)
                            continue;
                        var len = e.Audio.Body.Length;
                        if (len == 0)
                            continue;
                        bytesRead += len;
                        long toWrite = 0;
                        if (bytesRead > bytesFrom)
                        {
                            toWrite = bytesRead - bytesFrom;
                        }
                        if (toWrite > len)
                            toWrite = len;

                        int bodyCpyPos = (int)(len - toWrite);
                        int bodyCpyBytes = (int)(bytesLeaving >= toWrite ? toWrite : bytesLeaving);
                        if (bodyCpyBytes == 0)
                            continue;

                        if (bodyCpyBytes == len)
                        {
                            ret.Events_.Add(e);
                        }
                        else
                        {

                            var b = e.Audio.Body.ToArray();
                            ret.Events_.Add(
                                new Event
                                {
                                    Audio = new Event.Types.Audio { Body = Google.Protobuf.ByteString.CopyFrom(b, bodyCpyPos, bodyCpyBytes) }
                                }
                                );
                        }
                        bytesLeaving -= bodyCpyBytes;

                        if (bytesLeaving == 0)
                            break;
                    }
                    if (ret.Events_.Count > 0)
                        await yield.ReturnAsync(ret);

                    if (bytesLeaving == 0)
                        break;
                }
            });
        }
        public static Dasync.Collections.AsyncEnumerable<Events> ViaAudioFetchLogger(this Dasync.Collections.AsyncEnumerable<Events> audioSource, long bitRate,CancellationToken breaker = default(CancellationToken))
        {

            return new AsyncEnumerable<Events>(async yield =>
            {
                long lastPrinted = 0;
                long totalBytes = 0;
                long duration = 0;
                _logger.LogInformation($"Audio fetch start: {TimeSpan.FromTicks(duration)}");
                
                var source = audioSource.GetAsyncEnumerator();

                while (await source.MoveNextAsync())
                {
                    var ev = source.Current;
                    foreach (var e in ev.Events_)
                    {
                        if (e.BodyCase != Event.BodyOneofCase.Audio)
                            continue;
                        totalBytes += e.Audio.Body.Length;
                        duration=(totalBytes * 80000000) / bitRate;
                        if ((duration - lastPrinted) >= 600000000)
                        {
                            lastPrinted = duration;
                            _logger.LogInformation($"Audio fetch continue: {TimeSpan.FromTicks(duration)}");
                        }
                        
                    }
                    await yield.ReturnAsync(ev);
                }
                _logger.LogInformation($"Audio fetch stop: {TimeSpan.FromTicks(duration)}");
            });
        
        }
    }
}
