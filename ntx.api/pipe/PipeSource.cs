﻿using cz.ntx.proto.v2t.engine;
using System;
using System.Threading.Tasks;
using Dasync.Collections;
using System.IO;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using System.Net.Http;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using ntx.api.pipe.events;
using System.Linq;

namespace ntx.api.pipe
{
    public static partial class PipeSource
    {
        public static AsyncEnumerable<string> LinesFromText(Stream stream, CancellationToken token = default(CancellationToken))
        {
            return new AsyncEnumerable<string>(async yield =>
            {
                StreamReader _stream = null;

                while (true)
                {
                    try
                    {
                        if (_stream == null)
                        {
                            _stream = new StreamReader(stream, new UTF8Encoding());
                        }
                        var line = await Task.Run(() => _stream.ReadLineAsync(), token);
                        if (line == null)
                            break;
                        var cancellationToken = yield.CancellationToken;
                        await yield.ReturnAsync(line);
                    }
                    catch (Exception ex)
                    {
                        if (token.IsCancellationRequested)
                            break;
                        else
                            throw ex;
                    }
                }
            });
        }
        public static AsyncEnumerable<T> FromList<T>(System.Collections.Generic.List<T> list, CancellationToken token = default(CancellationToken)) where T : Google.Protobuf.IMessage, new()
        {
            return new AsyncEnumerable<T>(async yield =>
            {
                //token.Register(() => yield.Break());
                foreach (var i in list)
                {

                    await yield.ReturnAsync(i);
                    if (token.IsCancellationRequested)
                        break;
                }
            });
        }

        public static AsyncEnumerable<T> FromEnumerator<T>(System.Collections.Generic.IEnumerator<T> enumerator, CancellationToken token = default(CancellationToken)) where T : Google.Protobuf.IMessage, new()
        {
            return new AsyncEnumerable<T>(async yield =>
            {
                //token.Register(() => yield.Break());
                while (enumerator.MoveNext())
                {
                    await yield.ReturnAsync(enumerator.Current);
                    if (token.IsCancellationRequested)
                        break;
                }
            });
        }

        public static AsyncEnumerable<T> FromProtoJsonStream<T>(Stream stream, CancellationToken token) where T : Google.Protobuf.IMessage, new()
        {
            return new AsyncEnumerable<T>(async yield =>
            {
                StreamReader _stream = null;

                while (true)
                {
                    try
                    {
                        if (_stream == null)
                        {
                            _stream = new StreamReader(stream, new UTF8Encoding());
                        }
                        var line = await Task.Run(() => _stream.ReadLineAsync(), token);
                        if (line == null)
                            break;
                        var cancellationToken = yield.CancellationToken;
                        await yield.ReturnAsync(Google.Protobuf.JsonParser.Default.Parse<T>(line));
                    }
                    catch (Exception ex)
                    {
                        if (token.IsCancellationRequested)
                            break;
                        else
                            throw ex;
                    }
                }

            });
        }


        public static AsyncEnumerable<T> FromProtoBinaryStream<T>(Stream stream, CancellationToken token) where T : Google.Protobuf.IMessage, new()
        {
            return new AsyncEnumerable<T>(async yield =>
            {
                Stream _stream = stream;

                while (true)
                {
                    try
                    {

                        var buffer = new byte[4];
                        var br = await _stream.ReadAsync(buffer, 0, 4);
                        if (br < 4)
                            break;
                        var size = BitConverter.ToInt32(buffer, 0);
                        var data = new byte[size];
                        br = await _stream.ReadAsync(data, 0, size);
                        if (br < size)
                            throw new Exception("Invalid stream");
                        var t = new T();
                        t.MergeFrom(new Google.Protobuf.CodedInputStream(data));
                        var cancellationToken = yield.CancellationToken;
                        await yield.ReturnAsync(t);
                    }
                    catch (Exception ex)
                    {
                        if (token.IsCancellationRequested)
                            break;
                        else
                            throw ex;
                    }
                }

            });
        }

        public static AsyncEnumerable<Events> EventsFromBinaryStream(Stream stream, int chunkSize = 4096, CancellationToken token = default(CancellationToken))
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                Stream _stream = null;
                bool loop = true;
                while (loop)
                {
                    try
                    {
                        if (_stream == null)
                        {
                            _stream = stream;
                        }
                        var buffer = new byte[chunkSize];
                        //var cancellationToken = yield.CancellationToken;
                        int bytes_read = 0;
                        while (bytes_read < buffer.Length)
                        {
                            int br = await _stream.ReadAsync(buffer, bytes_read, buffer.Length - bytes_read, token);
                            if (br < 1)
                            {
                                loop = false;
                                break;
                            }
                            bytes_read += br;
                        }
                        if (bytes_read > 0)
                        {
                            var ev = new Events();
                            ev.Events_.Add(new Event { Audio = new Event.Types.Audio() { Body = Google.Protobuf.ByteString.CopyFrom(buffer, 0, bytes_read) } });
                            await yield.ReturnAsync(ev);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (token.IsCancellationRequested)
                            break;
                        else
                            throw ex;
                    }
                }
            });
        }


        public static AsyncEnumerable<cz.ntx.proto.license.Task> TasksFromStream(Stream stream, CancellationToken token = default(CancellationToken))
        {
            return new AsyncEnumerable<cz.ntx.proto.license.Task>(async yield =>
            {
                StreamReader _stream = new StreamReader(stream, new UTF8Encoding());
                var line = await Task.Run(async () => await _stream.ReadToEndAsync(), token);
                var content = cz.ntx.proto.license.VersionedTasks.Parser.ParseJson(line);
                foreach (var t in content.Tasks)
                {
                    var cancellationToken = yield.CancellationToken;
                    await yield.ReturnAsync(t);
                }
            });
        }



        public static AsyncEnumerable<cz.ntx.proto.license.Task> TasksFromLocalStore(Func<Task<auth.Authorization.TaskAccessToken>> tokenFactory, CancellationToken breaker = default(CancellationToken))
        {
            return new AsyncEnumerable<cz.ntx.proto.license.Task>(async yield =>
            {
                var token = await tokenFactory();
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.Token);
                    StreamReader _stream = new StreamReader(await httpClient.GetStreamAsync(token.StoreEndpoint));
                    var line = await Task.Run(async () => await _stream.ReadToEndAsync(), breaker);
                    var content = cz.ntx.proto.license.VersionedTasks.Parser.ParseJson(line);
                    foreach (var t in content.Tasks)
                    {
                        var cancellationToken = yield.CancellationToken;
                        await yield.ReturnAsync(t);
                    }
                }
            });
        }

        public static AsyncEnumerable<Events> EventsFromRawTextStream(Stream stream, CancellationToken token, string newLine = "")
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                using (var reader = new StreamReader(stream))
                {
                    string line = null;

                    while ((line = await reader.ReadLineAsync()) != null)
                    {
                        line += newLine;
                        if (line.StartsWith("#"))
                            continue;
                        var events = new Events();
                        events.Events_.Add(new Event { Label = new Event.Types.Label { Item = line } });
                        if (events.Events_.Count > 0)
                        {
                            await yield.ReturnAsync(events);
                        }
                    }

                }
            });
        }
        private static void RemoveEmptyParagraphs(this XDocument doc)
        {
            System.Collections.Generic.List<XElement> _empty = new System.Collections.Generic.List<XElement>();
            foreach (var par in doc.XPathSelectElements("//pa"))
            {
                var text = "";
                foreach (var p in par.XPathSelectElements("./p"))
                {
                    text += p.Value;
                }
                if (text.Trim().Length == 0)
                {
                    _empty.Add(par);
                }
            }
            foreach (var e in _empty)
            {
                e.Remove();
            }
        }
        private static System.Collections.Generic.Dictionary<string,string> GetSpeakerMap(XElement speakers)
        {
            var ret = new System.Collections.Generic.Dictionary<string, string>();
            foreach (var spk in speakers.XPathSelectElements("//s"))
            {
                var name = spk.Attribute("firstname").Value + " " + spk.Attribute("surname").Value;
                ret[spk.Attribute("id").Value] = name.Trim();
            }
            return ret;
        }
        public static AsyncEnumerable<Events> ResFromTrsxStream(Stream stream, CancellationToken token, long from=0, long to=long.MaxValue)
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
            using (var reader = new StreamReader(stream))
            {
                XDocument doc = XDocument.Load(reader, LoadOptions.PreserveWhitespace);
                //doc.RemoveEmptyParagraphs();
                var paragraphs = doc.XPathSelectElements("//pa");
                var speakers = GetSpeakerMap(doc.XPathSelectElement("//sp"));
                var lastSpkId = "";

                System.Collections.Generic.HashSet<long> ts = new System.Collections.Generic.HashSet<long>();
                foreach (var par in paragraphs)
                {
                    var events = new Events();


                    //var spkId = "S" + XmlConvert.ToUInt32(par.Attribute("s").Value).ToString("000000");
                    var spkId = "anonym";

                    try
                    {
                        spkId = speakers[par.Attribute("s").Value];
                    }
                    catch
                    {

                    }

                    var bstart = XmlConvert.ToTimeSpan(par.Attribute("b").Value).Ticks;
                    var bend = XmlConvert.ToTimeSpan(par.Attribute("e").Value).Ticks;

                    if (bend < from)
                        continue;
                    if (bstart > to)
                        continue;

                    if (!ts.Contains(bstart))
                    {
                        events.Events_.Add(new Event { Timestamp = new Event.Types.Timestamp { Timestamp_ = (ulong)bstart } });
                        ts.Add(bstart);
                    }

                    // if (spkId != lastSpkId)
                    {
                        events.Events_.Add(new Event { Label = new Event.Types.Label { Speaker = spkId } });
                        await yield.ReturnAsync(events);
                        events = new Events();
                        lastSpkId = spkId;
                    }
                    bool fp = true;
                    foreach (var p in par.XPathSelectElements("./p"))
                    {
                        var b = XmlConvert.ToTimeSpan(p.Attribute("b").Value).Ticks;
                        var e = XmlConvert.ToTimeSpan(p.Attribute("e").Value).Ticks;
                        var t = p.Value;
                        if (e <= from || b >= to)
                            continue;
                        bool isNoise = Regex.IsMatch(t, @"\[\S::\S+\]");
                            
                            if (fp && !isNoise)
                            {
                                t = " " + t;
                                fp = false;
                            }

                            if (!ts.Contains(b))
                            {
                                events.Events_.Add(new Event { Timestamp = new Event.Types.Timestamp { Timestamp_ = (ulong)b } });
                                ts.Add(b);
                            }

                            
                            if(isNoise)
                            {
                                events.Events_.Add(new Event { Label = new Event.Types.Label { Noise = t } });
                            }
                            else
                            {
                                events.Events_.Add(new Event { Label = new Event.Types.Label { Item = t } });
                            }
                            

                            if (!ts.Contains(e))
                            {
                                events.Events_.Add(new Event { Timestamp = new Event.Types.Timestamp { Timestamp_ = (ulong)e } });
                                ts.Add(e);
                            }

                        }

                        if (!ts.Contains(bend))
                        {
                            events.Events_.Add(new Event { Timestamp = new Event.Types.Timestamp { Timestamp_ = (ulong)bend } });
                            ts.Add(bend);
                        }
                        await yield.ReturnAsync(events);
                    }


                }
            });
        }

        public class SpeakerBlock
        {
            public long Begin { get; set; }
            public long End { get; set; }
            public string Speaker { get; set; }
            public System.Collections.Generic.List<Events> Events { get; set; }
            public bool HasTranscription { get { return Events != null;} }
            public long Duration { get { return End - Begin; } }
        }

        public static System.Collections.Generic.IEnumerable<Event> ToEventList(
            this System.Collections.Generic.IEnumerable<SpeakerBlock> self, long start, long end)
        {
            var ret = new System.Collections.Generic.List<Event>();
            bool pass = false;
            foreach (var spk in self)
            {

                var l = spk.Events;
                if( l == null) {

                    var _ev = new Events();
                    _ev.Events_.Add(new Event() { Timestamp = new Event.Types.Timestamp() { Timestamp_ = (ulong)spk.Begin } });
                    _ev.Events_.Add(new Event() { Timestamp = new Event.Types.Timestamp() { Timestamp_ = (ulong)spk.End } });

                    l = new System.Collections.Generic.List<Events>() { _ev};
                }
                bool f = true;
                foreach (var e in l)
                {
                    foreach (var ee in e.Events_)
                    {
                        if (ee.BodyCase == Event.BodyOneofCase.Timestamp 
                            && ee.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Timestamp_)
                        {
                            if(!pass && ee.Timestamp.Timestamp_ >= (ulong)start)
                            {
                                pass = true;
                            }
                            if (pass && ee.Timestamp.Timestamp_ >= (ulong)end)
                            {
                                ret.Add(ee);
                                return ret;
                            }

                        }
                        if (!pass)
                            continue;

                        if (f && ee.BodyCase == Event.BodyOneofCase.Label)
                        {
                            ret.Add(new Event() { Label = new Event.Types.Label() { Plus = " " } });
                            f = false;
                        }

                        ret.Add(ee);
                    }
                }
            }
            return ret;
        }

        public static System.Collections.Generic.IEnumerable<SpeakerBlock> GetOuter(this System.Collections.Generic.IEnumerable<SpeakerBlock> self,long start, long end){

            return self.Where(x => x.Begin < end && x.End > start);
        }
        public static System.Collections.Generic.IEnumerable<SpeakerBlock> GetInner(this System.Collections.Generic.IEnumerable<SpeakerBlock> self, long start, long end)
        {

            return self.Where(x => x.Begin >= start && x.End <= end);
        }
        public static System.Collections.Generic.IEnumerable<long> ToSpkTurns(this System.Collections.Generic.IEnumerable<SpeakerBlock> self)
        {
            var hash = new System.Collections.Generic.HashSet<long>();
            foreach ( var i in self)
            {
                hash.Add(i.Begin);
                hash.Add(i.End);
            }
            return hash.ToList().OrderBy(x => x);
        }

        public static AsyncEnumerable<SpeakerBlock> FromTrsxStream(Stream stream, CancellationToken token, Regex split, Regex plus, bool rmZeroDuration)
        {
            return new AsyncEnumerable<SpeakerBlock>(async yield =>
            {
                using (var reader = new StreamReader(stream))
                {

                    XDocument doc = XDocument.Load(reader, LoadOptions.PreserveWhitespace);
                    doc.RemoveEmptyParagraphs();
                    var paragraphs = doc.XPathSelectElements("//pa");
                    long duration = XmlConvert.ToTimeSpan(doc.XPathSelectElements("//pa").Last().Attribute("e").Value).Ticks;

                    System.Collections.Generic.HashSet<long> ts = new System.Collections.Generic.HashSet<long>();
                    long expectedStart = 0;
                    foreach (var par in paragraphs)
                    {


                        var bb = XmlConvert.ToTimeSpan(par.Attribute("b").Value).Ticks;
                        var be = XmlConvert.ToTimeSpan(par.Attribute("e").Value).Ticks;
                        
                        var emptyBlock = new SpeakerBlock
                        {
                            Begin = expectedStart,
                            End = bb,
                            Speaker = null,
                        };
                        expectedStart = be;

                        if (rmZeroDuration)
                        {
                            if (emptyBlock.Duration > 0)
                                await yield.ReturnAsync(emptyBlock);
                        }
                        else
                            await yield.ReturnAsync(emptyBlock);

                        var events = new Events();
                        events.Events_.Add(new Event { Timestamp = new Event.Types.Timestamp { Timestamp_ = (ulong)bb } });
                        ts.Add(bb);
                        foreach (var p in par.XPathSelectElements("./p"))
                        {
                            var b = XmlConvert.ToTimeSpan(p.Attribute("b").Value).Ticks;
                            var e = XmlConvert.ToTimeSpan(p.Attribute("e").Value).Ticks;
                            var t = p.Value;
                            if (!ts.Contains(b))
                            {
                                events.Events_.Add(new Event { Timestamp = new Event.Types.Timestamp { Timestamp_ = (ulong)b } });
                                ts.Add(b);
                            }

                            events.Events_.Add(new Event { Label = new Event.Types.Label { Item = t } });

                            if (!ts.Contains(e))
                            {
                                events.Events_.Add(new Event { Timestamp = new Event.Types.Timestamp { Timestamp_ = (ulong)e } });
                                ts.Add(e);
                            }
                            
                            
                        }
                        if (!ts.Contains(be))
                        {
                            events.Events_.Add(new Event { Timestamp = new Event.Types.Timestamp { Timestamp_ = (ulong)bb } });
                            ts.Add(be);
                        }


                            var block = new SpeakerBlock {
                                Begin = bb,
                                End = be,
                                Speaker = "S" + XmlConvert.ToUInt32(par.Attribute("s").Value).ToString("000000"),
                                Events = await PipeSource.FromList(new System.Collections.Generic.List<Events>() {
                                            events
                                        }).ViaLookAheadFilter().ViaReLabelAndSplit(split, plus).ViaLabelMerge()
                                        .ToListAsync()
                        };
                        


                        

                        await yield.ReturnAsync(block);
                    }
                    


                }
            });
        }

        public static AsyncEnumerable<Events> RefFromTrsxStream(Stream stream, CancellationToken token)
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                using (var reader = new StreamReader(stream))
                {
                    
                    XDocument doc = XDocument.Load(reader,LoadOptions.PreserveWhitespace);
                    //doc.RemoveEmptyParagraphs();
                    var paragraphs = doc.XPathSelectElements("//pa");
                    var speakers = GetSpeakerMap(doc.XPathSelectElement("//sp"));
                    var lastSpkId = "";
                    long lastEnd = -1;
                    foreach (var par in paragraphs)
                    {
                        var events = new Events();
                        var bstart = XmlConvert.ToTimeSpan(par.Attribute("b").Value).Ticks;
                        var bend = XmlConvert.ToTimeSpan(par.Attribute("e").Value).Ticks;
                        var spkId = "anonym";

                        try
                        {
                            spkId = speakers[par.Attribute("s").Value];
                        }
                        catch
                        {

                        }
                        //if (spkId != lastSpkId)
                        {

                            events.Events_.Add(new Event { Label = new Event.Types.Label { Speaker = spkId } });
                            lastSpkId = spkId;
                        }
                        if (lastEnd < 0)
                        {
                            events.Events_.Add(new Event { Timestamp = new Event.Types.Timestamp { Timestamp_ = (ulong)bstart } });
                            await yield.ReturnAsync(events);
                            events = new Events();
                        }
                        /*
                        else if(lastEnd != bstart)
                        {
                            var no_trans = " NO_TRANS ";
                            events.Events_.Add(new Event { Timestamp = new Event.Types.Timestamp { Timestamp_ = (ulong)bstart } });
                            events.Events_.Add(new Event { Label = new Event.Types.Label { Item = no_trans } });
                            events.Events_.Add(new Event { Timestamp = new Event.Types.Timestamp { Timestamp_ = (ulong)bend } });
                            await yield.ReturnAsync(events);
                            events = new Events();
                        }
                        */

                        lastEnd = bend;

                        //var spkId = "R" + ((uint)XmlConvert.ToInt32(par.Attribute("s").Value)).ToString("000000");
                        
                        string line = "";
                       
                        bool fp = true;
                        foreach (var p in par.XPathSelectElements("./p"))
                        {
                            var t = p.Value;
                            if (fp)
                            {
                                t = " " + t;
                                fp = false;
                            }
                            line += t;
                        }

                        
                        events.Events_.Add(new Event { Label = new Event.Types.Label { Item = line } });

                        await yield.ReturnAsync(events);
                    }
                    var events1 = new Events();
                    events1.Events_.Add(new Event { Timestamp = new Event.Types.Timestamp { Timestamp_ = (ulong)lastEnd } });
                    await yield.ReturnAsync(events1);


                }
            });
        }
    }
}

