﻿using Dasync.Collections;

namespace ntx.api.pipe.tasks
{
    public static partial class Extensions
    {
        public static Dasync.Collections.AsyncEnumerable<string> ToStringChunkStream(this Dasync.Collections.AsyncEnumerable<cz.ntx.proto.license.Task> taskItemSource)
        {

            return new AsyncEnumerable<string>(async yield =>
            {
                var source = taskItemSource.GetAsyncEnumerator();
                while (await source.MoveNextAsync())
                {
                    foreach (var v in source.Current.Profiles)
                    {
                        foreach (var l in v.Labels)
                        {
                            await yield.ReturnAsync($"{source.Current.Service} -l {l} -t {string.Join(" -t ", source.Current.Tags)}" + System.Environment.NewLine);
                        }
                    }
                }
            });
        }
    }
}
