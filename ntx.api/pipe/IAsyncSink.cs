﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ntx.api.pipe
{
    public interface IAsyncSink<T>
    {
        Task WriteAsync(T item);
        Task CompleteAsync();
    }
}
