﻿Věci veřejné a další pokus o diktát. Pokud vládní partneři do konce měsíce nesplní jejich podmínky, z kabinetu odejdou.
Považujeme za vyloučené, aby ve vládě prostě seděli lidé, kteří neuhlídali veřejné peníze.
Personální změny nemají být dělány pro pouhé personální změny.
Nejmenší strana v kabinetu zase požaduje dodatek koaliční smlouvy.
Podrobnosti i novém kole rozporů hned v úvodu po přehledu.
Po šestnácti letech konec v ředitelně Sazky a omezený přístup do budovy. 
Proti Aleši Hušákovi zakročil insolvenční správce. 
Nové zjištění České televize o místě nedělní nehody vlaku. 
Podnět opravit kritické místo měl správce kolejí už před rokem. 
Do kauzy vstoupil i ministr dopravy.
Až dvacet tisíc propadlých u státní maturity.
Odborníkům se dvojnásobný nárůst reparátů proti minulosti nelíbí. 
devatenáct nula nula. Čas Událostí, dobrý večer. I ode mě. 
Ani sedmý červen nerozsekl krizi vlády.
Věci veřejné vytkly další datum D.
Tentokrát do konce měsíce žádají změny v kabinetu nebo třeba zákaz anonymních akcí.
Všechno sepsané v dovětku koaliční smlouvy. 
Jinak odcházejí. 
Premiér kontroval, že se k výměně ministrů tlačit nenechá a hlavně, že podstatné jsou pro něj reformy. 
A právě ty začala schvalovat sněmovna. 
Na místě je Jana Čermáková. Jano, otázka je jasná. Co už prošlo? 

Hezký večer. 
Tak já musím v úvodu obecně konstatovat, že dopady té vládní krize na projednávání jednotlivých reformních zákonů tady v Poslanecké sněmovně zatím vidět nejsou.
Vládním poslancům se už podařilo propustit do dalšího projednávání zákon o veřejných zakázkách, který má omezit korupci při zadávání veřejných zakázek a zprůhlednit ono zadávání.
Pak taky zákon o stavebním spoření, který má zkrátit příspěvek státu na stavební spoření a teď už nějakou dobu poslanci jednají o změnách v dani z přidané hodnoty.
Opozice statečně kritizuje a nesouhlasí, ale to je to jediné, co teď v Poslanecké sněmovně zmůže. 

Koaličním poslancům zatím stále prochází to, na čem se domluvili.
Nicméně ovšem to neznamená, že by vládní krize skončila.
Vládní krize pokračuje, Věci veřejné přišly s dalším ultimátem a trvají si na něm.
pět hodin vydrželi v téhle místnosti rokovat poslanci Věcí veřejných.
Nakonec vymysleli další ultimátum, s nímž vyrazili za koaličními partnery.
S těmi se pak zdrželi pár minut.
Nešlo v podstatě o jednání.
Sdělili jsme našim koaličním partnerům výsledky jednání poslaneckého klubu.
My pouze suše konstatujeme, že bereme tento postoj na vědomí. Žádná změna.
Všichni trvají na svém a dál se zaklínají reformami.
Vláda tak získala jediné - čas.
Koaliční zákony by měly na právě odstartované sněmovní schůzi procházet bez potíží.
Věci veřejné vypovídají koaliční smlouvu k třicátému červnu roku dva tisíce jedenáct, pokud nebude do té doby uzavřen písemný dodatek.
Co přesně by v něm mělo být, ale véčkaři neříkají.
Odečíst lze třeba to, že chtějí hlavu Alexandra Vondry, čtvrté křeslo ve vládě, zřejmě víc ministerských náměstků a zrušení takzvaných anonymních akcí.
Když v tom potom jste v té koalici, tak musíte být rezolutní a zároveň nějak odpovědní my se v tom snažíme nějak bruslit.
Neví úplně přesně, co chtějí, ale zjevně nedají pokoj, dokud toho nedosáhnou.
Je skutečností, že my ne zcela chápeme, co konkrétně tedy má být předmětem onoho dodatku.
Z opozičních řad jdou úšklebky i výroky o politických nulách.
Cítit je i roztrpčení, že předčasné volby jen tak nebudou.
Věci veřejné vydaly asi tisící třísté osmdesáté sedmé vážné varování.
My už jsme se těšili jako sociální demokraté, že teď ve čtrnáct hodin konečně tato vládní koalice skončí a budeme moci začít jednat o nových volbách.
Věci veřejné chtějí o dodatku ke koaliční smlouvě jednat ještě tento týden.
Další ze série schůzek stranických lídrů tak bude možná už zítra. Štěpánka Martanová a Jan Šenkýř, Česká televize.
Co vlastně chtějí Věci veřejné za to, že neodejdou na konci června z vlády? Co to přesně znamená sebereflexe koalice? Co by mělo být v dodatku ke koaliční smlouvě a jak by měla vypadat očista koalice? Otázky pro předsedu Věcí veřejných Radka Johna, který bude hostem Událostí, komentářů ve dvacet dva hodin.
Aleš Hušák skončil v čele Sazky a má taky omezený vstup do loterijní společnosti.
Z funkce ho odvolal insolvenční správce Josef Cupka.
Teď už bývalý generální ředitel tak zůstává jen předsedou představenstva.
Přímo před sídlem firmy je kolega Jakub Linka.
Jakube, jaký to odvolání mělo průběh a co Sazku v nejbližších dnech čeká? Dobrý večer.
Tak to odvolání určitě v klidu neproběhlo.
Mluvčí insolvenčního správce Lenka Tichá nám řekla, že Aleš Hušák odmítl zprávu o svém odvolání převzít.
Aleš Hušák zase tvrdí, že o ničem neví a chystá další protikroky.
Ostatně na reakce obou stran se můžete teď podívat.
Pak doktor Hušák vlastně odmítl osobně odvolání převzít.
Doktor Aleš Hušák upozorňuje, že právní moc konkurzu stále nenastala.
To znamená, že konkurz není pravomocný a zítra proti němu bude podáno odvolání.
Ta situace není teď vůbec jednoduchá.
Sazka se snaží získat zpátky důvěru sázejících.
Měla totiž problémy s vyplácením stomiliónové výhry.
Teď už získala dvousetmilionový úvěr od firmy Moranda ze skupiny PPF, takže dluhy splatila, sázející se opět nebojí sázet a vracejí se.
Sazka říká, že její tržby se zvedly až o dvacet procent.
Na šestnáctileté působení Aleše Hušáka v čele největší loterijní společnosti u nás se můžete podívat v následujícím příspěvku.
Hušák začal svým nadřízeným, tedy sportovním svazům velet pevnou rukou v roce devatenáct set devadesát pět.
Sazka vydělávala a sportovcům posílala stovky miliónů ročně.
Bez nás by zhasly tělocvičny, spousta tělocvičen bez nás by nefungovala údržba, bez nás by třeba v Písku nejezdili mladí atleti prostě autobusem.
Pak ale chtělo Česko hostit mistrovství světa v ledním hokeji.
Vhodná hala chyběla a Hušák zavelel.
Já hokejový stánek postavím.
Tentokrát ale vláda vstříc nevyšla a kabinet Vladimíra Špidly garanci neposkytl.
V roce dva tisíce čtyři se aréna otevřela i s cedulí a textem proti vůli mnohých a navzdory mnohým.
Původně byly náklady na stavbu arény spočítány na dvě a půl miliardy korun.
Na začátku letošního roku ale už dosáhly sedmnácti miliard.
Splácet vypůjčené peníze bylo čím dál těžší.
Leden dva tisíce jedenáct přinesl zvrat.
Miliardář Radovan Vítek začal skupovat pohledávky a svým návrhem poslal Sazku do insolvence.
Ono mě to baví.
Sazka nutně potřebuje peníze.
Má problémy i s vyplacením více než stomilionového jackpotu.
Ministerstvo financí zkoumá, jestli společnost nezbaví loterijní licence.
Kolem loterijní společnosti krouží finanční skupiny.
Zájem o ni má Penta spolu s E-Investem, ale i KKCG a skupina PPF.
Aleš Hušák je mimořádně silný, schopný člověk, ale zapomněl, že žádný strom neroste do nebe.
Posledně dva jmenovaní jsou největšími věřiteli Sazky.
Po vyhlášení konkurzu přebírá nad Sazkou moc insolvenční správce Josef Cupka.
Aleše Hušáka opouštějí jeho skalní spolupracovníci a jeho příběh, zdá se, pomalu končí.
Hana Vorlíčková a Miloš Minařík, Česká televize.
Zřejmě zakletý úsek kolejí.
Po nedělní nehodě u Vyškova vyšlo teď najevo, že závady se na trati se objevily víc než před rokem.
Správa železniční dopravní cesty je ale navzdory vlastním předpisům neodstranila.
Do případu se už kriticky vložil i ministr dopravy.
Dobrý o tom vybočení v Topolanech? V mém směru je to tak asi doleva vybočené, obě dvě ty koleje.
Hlášení o nebezpečí, které si nahrál mobilním telefonem jeden ze strojvedoucích, přestože správce kolejí, SŽDC, od pátku varování přijímal, rychlost vlakům až do havárie nesnížil.
Posádky se proto aspoň varovaly navzájem.
Strojvedoucí víc ne, no.
Kdyby to jeli rychleji, asi to tam cosi hodí.
Ti chlapi prostě se zachovali jako profíci.
Díky jim se nestalo vlastně neštěstí.
Jen od pátečního prvního hlášení ve třináct hodin, že trať není v pořádku, až do nedělní nehody po kolejích projelo čtyři a dvacet osobních vlaků a osmdesát rychlíků plných lidí.
Ti všichni byli v ohrožení.
Trať v kritickém stavu.
devadesát osm procent uvolněných upevňovacích šroubů.
Prokázali jsme těmito záběry.
Jenže stav byl ještě horší.
V místě byly i vady kolejnic, které správce kolejí měl opravit a neopravil.
Bylo zjištěno před rokem přibližně sto šestnáct defektoskopických vad, které měly být odstraněny do půl roku a ani po půl roce odstraněny nebyly.
I toto, že nebyly opraveny, je v současné době předmětem detailního vnitřního vyšetřování, na jehož konci mohou padnout i konkrétní lidé, kteří nesou trestněprávní zodpovědnost.
Do případu teď poprvé zasahuje taky ministr dopravy Radek Šmerda.
Vadí mu, že si Správa železniční dopravní cesty vyměňuje s Drážní inspekcí obvinění přes média.
V tom resortu je někde problém mezi vyšetřovatelem a řekněme tím provozovatelem dráhy.
Ministr dopravy na základě té vyhrocené diskuze, která se vede přes média, si pozve oba dva manažery.
Trať Brno-Přerov-Ostrava by měla být po dvou a půl denním kolapsu znovu plně zprovozněna od středečního rána.
Luboš Rosí, Česká televize.
I čeští zemědělci dostanou odškodnění za ztráty kvůli panice související s nákazou bakterií E coli.
Dohodli se na tom ministři zemědělství sedmadvacítky v Lucemburku.
Peníze půjdou z evropské pokladny z přihrádky na krize.
Původní návrh byl sto padesát milionů eur pro celou Unii.
Nakonec ale bude částka nejspíš vyšší.
Nejhorší okurková sezóna.
Právě teď ji prožívá jejich nizozemský pěstitel Koos de Vries.
Je na pokraji bankrotu.
Většinu okurek ze svých skleníků vyváží k sousedům.
V Německu do nich ale na rozdíl od něj málo kdo má teď odvahu kousnout.
Musí co nejdřív zjistit, odkud se infekce šíří.
Chceme dokázat, že naše plody jsou v pořádku, ale dokud Němci nebudou vědět, co je za infekci, neuvěří nám.
Nejde jen o holandské pěstitele a o německé konzumenty.
Panika jménem E coli i ostatní zemědělce stojí milióny. Španělsko mluví o ztrátách dvě stě miliónů eur týdně.
Nizozemsko o osmdesáti, Německo o dvaceti a Belgie o třech miliónech eur.
Nejdřív to byly okurky, rajčata, saláty, pak zeleninové výhonky.
Příčina nákazy je stále neznámá.
Neprodaná zelenina už ale teď je testem evropské solidarity.
Peníze nakonec půjdou do všech zemí.
Teď jde ale o seznam postižené zeleniny.
Okurky, rajčata a saláty už z něj nezmizí a taky o výši kompenzací.
Určitě to nebude, nebude sto procent, určitě to nebude třicet procent, to číslo bude se bude pohybovat někde mezi tím.
Politici o obnovení důvěry v evropské okurky usilují stůj co stůj.
Další nápady padaly v europarlamentu.
Navrhuji, abychom ve Štrasburku nebo v Bruselu uspořádali přehlídku evropské gastronomie zaměřenou hlavně na okurky.
Omezení prodeje jakékoli zeleniny podle Bruselu není nutné.
Eurokomisař zdůrazňoval, že nákaza se týká jen okolí Hamburku.
Informace o ní se ale šíří mnohem rychleji a po celé Evropě.
Z Bruselu a Lucemburku Eva Hrnčířová, Česká televize.
Hradec Králové přišel o část dotace na zateplení škol kvůli chybným údajům v projektech.
Firma Tenderservis, která žádost pro město zpracovala, přitom nakonec inkasovala o dva miliony vyšší odměnu, než byla ta původně dohodnutá.
Hradec zatepluje dvě základní školy vybrané z osmnácti. Žádosti o dotace a projekty připravila firma Tenderservis z Pardubic.
Parametry budov v projektech ale podle radnice neodpovídají skutečnosti.
Zjistili jsme rozdíly ve výměře oken a výměře některých těch vstupních prostor a některých prosklených částí, zejména u tělocvičen.
V projektech jsou výměry menší než ve skutečnosti, to stěžuje stavbu, a nebýt chyb nebo úmyslu mohla být dotace vyšší, tvrdí radnice.
Já o žádných nedostatcích nevím.
Bývalý ekonomický náměstek Herman rozhovor odmítl a odkázal na podřízeného Hartmana.
Toho jsme se zeptali, proč má radnice od Tenderservisu místo osmnácti projektů na opravu škol, jak znělo zadání zakázky, projektů jen deset a proč dodavatel dostal k původní ceně šestnáct milionů dvě stě tisíc nečekaně přidáno, přes dva miliony dvě stě tisíc.
Na to vám neodpovím, tohle to šlo přes majetek a orgány města a nevím to.
To navýšení bylo za dodatečný práce, který nebyly ve smlouvě.
Magistrát bude firmu Tenderservis žádat o vrácení stovek tisíc korun.
Díky chybám v projektové dokumentaci město Hradec Králové dostalo méně peněz, než mohlo dostat, no, a ten vzniklý rozdíl budeme samozřejmě požadovat.
Kdyby zakázku Tenderservisu začal zkoumat antimonopolní úřad, mohlo by mít město další problémy, připustil Ondřej Votroubek.
Radnice projekty opravila, ale velmi opatrně a se souhlasem ministerstva životního prostředí, aby nepřišla o dotace.
Vlastimil Weiner, Česká televize, Hradec Králové.
Společnost Diag Human podala další návrhy na exekuci českého majetku ve světě.
Po vídeňských a pařížských soudech uznaly podle bývalého majitele firmy nároky na desetimiliardové odškodnění za zmařený obchod s krevní plazmou taky verdikty ve Velké Británii, Spojených státech a dalších zemí Evropské unie.
Asi tři sta lidí podpořilo protest proti demolici nárožního domu na Václavském náměstí.
Podle svolavatele klubu Za starou Prahu patří budova do památkové rezervace zařazené na seznam UNESCO a případné stržení přinese návod, jak se zbavit i dalších chráněných staveb.
Stavební firma Skanska žaluje ministerstvo dopravy a Ředitelství silnic a dálnic kvůli cedulím na místech, kde údajně provedla špatnou práci.
Požaduje odškodné padesát milionů korun.
Značení nechal nainstalovat bývalý ministr Vít Bárta.
Karlovy Vary zažalovaly sdružení tří firem, které postavily tamní multifunkční arénu.
Město chce zpátky přes miliardu korun, kterou do haly vložilo a zpochybňuje smlouvy o vlastnictví.
Dodavatelé i opozici v zastupitelstvu iniciativa překvapila.
O to víc, že ji neschválila rada města.
Stavba multifunkční arény v Karlových Varech byla od začátku v rozporu se zákony, říká náměstek primátora Jiří Kotek.
Podle něj to potvrdil svou pokutou i Úřad na ochranu hospodářské soutěže.
Podle právního zástupce radnice se tím pádem hala nikdy nestala majetkem města.
Pokud se poruší zadávání veřejné zakázky, tak smlouva takto uzavřená je neplatná.
To je předmětem naší žaloby, tedy určení vlastnického práva, a pakliže nepatří městu, ale patří právě těm nezákonně vybraným dodavatelům, pak by tedy měli vydat plnění, a to znamená tu miliardu.
Sdružení stavebních firem, Syner, Baustav a Metrostav, žaloba zaskočila.
Doposud jsme se s takovým druhem žaloby nesetkali.
Proto si necháváme zpracovat právní stanovisko.
Podle všech indicií to není nápad vedení města Karlovy Vary.
Jak ale zjistila Česká televize, žalobu poslal o své vůli náměstek primátora Jiří Kotek bez vědomí rady města.
Skandální chování prvního náměstka pana Kotka, který absolutně chaoticky podává žaloby na městský majetek.
Je naší politickou povinností a odpovědností poukázat na to zcela jednoznačně, že to, co se dělo, bylo nezákonné. Žalobu podal náměstek primátora Krajskému soudu v Plzni tři a dvacátého května. Řízení je ve fázi, kdy se řeší podmínky řízení.
Soud se tedy v této fázi ještě nezabývá důvodností žaloby.
V případě jejího úspěchu by firmy musely městu do tří dnů zaplatit miliardu a šedesát tři milionů korun.
Společně by se pak staly majitele KV Areny.
Marek Štětina, Česká televize.
Helena Vondráčková prohrála spor o čtrnáct milionů korun, které chtěla po státu.
Podle ní neochránil ji ani manžela před pomlouvačnou kampaní bulvárních médií.
Nejvyšší správní soud konstatoval, že Česko jí žádnou škodu nezpůsobilo a na peníze nemá nárok.
Nový zábavně vzdělávací portál TÝ YÓ pro děti a náctileté spustila Česká televize.
Nabízí tři různé sekce, Safari pro předškoláky, ponorky a Podmořský svět inspirovaný Julesem Vernem pro děti mezi osmi a jedenácti roky, a Vesmír pro starší školáky.
Stránka určená rodičům pak apeluje na bezpečné chování na síti.
Dobré zprávy pro pacienty s rakovinou kůže přinesla konference v americkém Chicagu.
Lékaři představili hned dva preparáty, které výrazně prodlužují život nemocných a fungují líp než dosud používaná chemoterapie.
Měsíc života, takovou prognózu si vyslechla Susan Steelová, má rakovinu čtvrtého stupně s napadenou slezinou, játry, plícemi i mozkem.
Neměla co ztratit, proto se přihlásila k testům nového léku Vemurafenib. Zabral.
Steelová nejen žije, ale rakovina jí ustoupila o sedmdesát procent.
Na levém boku jsem měla nádor jako míč.
Teď se zmenšil na velikost dlaně. Účinky nového léku překvapily i zdravotníky.
Jeho výsledky byly v prvních testech oproti zdravotně vyčerpávající chemoterapii o dvacet procent lepší.
sedm set pacientů v pokročilém stádiu rakoviny ho testovalo půl roku.
Přežilo čtyři a osmdesát procent z nich.
U poloviny se dokonce nádory výrazně zmenšily, všechno bez vedlejších účinků.
Pacienti, kterým jsme podávali Vemurafenib, do dva a sedmdesáti hodin projevili zlepšení.
Někteří se obešli bez léků proti bolesti.
Další jsme mohli odpojit od kyslíku.
Vemurafenib se zaměřuje na zmutovaný gen zvaný Braf, který má přibližně polovina nemocných a který podporuje rakovinné bujení.
Právě díky tomu dokáže i zmenšovat nádory.
Lékaři proto doufají, že další testy budou stejně úspěšné a neodhalí žádné vedlejší účinky.
Chceme lék začít zkoušet na pacientech už v prvních fázích onemocnění a doufáme, že tím dokážeme zabránit, aby se rakovina rozvinula do pokročilejších stádií.
Alespoň trochu dobré zprávy přicházejí i pacientům s rozvinutou rakovinou.
Nový lék Ipilimumab jim dokáže prodloužit život až o několik měsíců.
Lékaři přesto varují, že nejlepší cestou, jak proti rakovině bojovat, jsou pravidelná preventivní vyšetření.
Pavel Šimek, Česká televize.
Nepříliš povzbudivou zprávu se naopak dozvěděl majitel beskydského lesa.
Stačil jeden padělaný doklad, aby o něj přišel.
Příběh za moment.
Nelíbí se vám drobnosti do bytu, které se běžně prodávají, jsou bez nápadu, mají špatnou barvu nebo velikost? V brzké době si vše potřebné budete moct vytisknout.
Reportáž na konci.
Události, každý den v sedm večer na ČT jedna a ČT dvacet čtyři.
Každou informaci pečlivě prověříme.
Nám můžete věřit.
Máme zkušenosti.
A stojíme si za tím, co říkáme.
Jsme jediná zpravodajská televize s největší sítí redaktorů u nás i v zahraničí.
My jsme Události.
Poslední středoškoláci dopsali maturitní písemky.
Průběžné výsledky naznačují, že neuspěje pětina z nich.
Přesně, jak ministerstvo odhadlo.
Podle odborníků je číslo příliš vysoké.
Někdo září, někdo v září.
Oblíbené pořekadlo studentů, kterým se při maturitách vzájemně chlácholí.
Na podzim si zřejmě zkoušku zopakuje asi dvacet tisíc žáků, tedy pětina.
Pro srovnání, před jednotnými maturitami to byla jen necelá polovina.
Je to dáno tím, že ty testy a písemné práce jsou plošně objektivní a nezohledňují, jestli žák je na té či oné škole.
To číslo je dosti vysoké, protože pokud ty studenti vlastně mají určité problémy, tak možná vůbec neměli dojít někam do čtvrtého ročníku.
Na rozdíl od dřívějška se studenti výsledky dozví až sedmnáctého června.
I takhle vypadal na některých školách poslední den státních maturit.
Písemnou zkoušku tu skládali dva studenti z ruského jazyka.
Teďka vlastně budu čtrnáct dní čekat na ty výsledky, tak je to spíš taková nejistota.
Nejnovější už potvrzená čísla, u ústní části neuspělo pět procent studentů, desetina se jich vůbec nedostavila.
To znamená, že jenom tuhle zkoušku bude muset patnáct procent maturantů dělat až na podzim.
Máme jednoho studenta, který v ústní zkoušce neprospěl ve společné části, samozřejmě v těch profilových jich bylo více.
Právě tuhle zkoušku, která je v režii škol, bude muset opakovat šestnáct a půl procenta studentů.
Ministerstvo s tolika propadlíky počítalo.
I přesto se zatím neví, co bude v září s těmi, kteří chtěli na vysokou školu.
Josef Dobeš slibuje, že jim vyjedná podmínečné přijetí.
S rektory o tom ale ještě nemluvil.
Zatím to funguje nad očekávání dobře.
To nikdo nečekal a já jsem za to velmi vděčen. Školám už práce téměř skončila.
Zbývá už jenom odeslat data do centra pro přípravu maturit. Čas na to mají do čtvrtka.
Barbora Straňáková a Barbora Jelínková, Česká televize.
Další případ rafinovaného podvodu s nemovitostmi vyšetřuje policie.
Rudolf Fárek z Horní Bečky na Valašsku přišel ze dne na den o les, přitom nic neprodal.
Obchod za jeho zády udělal někdo jiný, na falešnou občanku i falešný podpis.
Vlastnil šest hektarů lesa v samém srdci Beskyd.
Teď nemá Rudolf Fárek nic.
Pozemek, který před dvaceti lety koupil, mu přestal říkat pane, přestože ho nikdy neprodal.
Někdo zfalšoval moji občanku a na základě toho získal na poště v Ostravě plnou moc.
Potom začal s majetkem si nakládat podle sebe.
V tu chvíli se zástupcem majitele stal jedna dvaceti-letý Jakub Beneš z Havířova, o kterém však majitel nikdy neslyšel a kterého podle informací České televize policie stíhá i za jinou trestnou činnost.
Oznámení prověřujeme, ale vzhledem k probíhajícímu šetření prozatím nebudeme podrobnější informace k případu zveřejňovat.
V této kauze sehrál Jakub Beneš roli takzvaného bílého koně.
Jako údajný prostředník cizí les prodal dál, zástupci firmy Melard, mimo jiné mladíci ve věku jednadvacet a dva a dvacet let se papírově ze dne na den stali majiteli pozemků v hodnotě pěti milionů korun.
V obchodním rejstříku je společnost zapsána od letošního čtyři a dvacátého března, vznikla tedy pouhý měsíc předtím, než k podvodu s pozemky došlo.
Podle obchodního rejstříku sídlí firma Melard v tomto domě na Praze deset.
Její název se ale objevuje na této dopisní schránce.
Jsem tady tři měsíce a slyším to poprvé.
Pravděpodobně jde o bílé koně, neboť firma Melard zastavila pozemky ihned na katastru budějovické firmě Yaris.
Společnost, která nabízí krátkodobé úvěry, se od celé věci distancuje.
Právoplatný majitel lesa Fárek podal trestní oznámení.
Policie po aktérech rafinovaného podvodu pátrá.
Je to v rukách božích, no.
Co můžeme dělat zatím.
Počet takových kauz přitom narůstá.
Koncem loňského roku poslal soud na šest let do vězení muže, který se pokusil zastavit cizí dům v centru Zlína.
Tereza Kručinská a Filip Zdražil, Česká televize.
Syrský režim bojuje o přežití.
Povstání v zemi přerostlo v otevřenou válku rebelů a vládních vojáků.
V posledních dnech zahynuly stovky lidí.
Ze severozápadu země, kde povstalci podle některých zpráv ovládli pár měst, prchají civilisti.
A svět stupňuje tlak na prezidenta Asada, aby odstoupil.
Sýrie se propadá do chaosu.
Kvůli vládní propagandě a chybějícím nezávislým zdrojům je v něm těžké se vyznat.
Náznaky velkého povstání proti vládě ale přicházejí ze všech stran.
Okolí velkých měst Homs, Hamá a Latakíja ovládají podle světových agentur povstalci.
Měli zaútočit na armádní sklady a výbušninami pak zničit mosty.
Vláda mluví o boji s teroristy.
Sýrie zažila v posledních dnech mnoho ozbrojených útoků na vládní, veřejné i soukromé cíle.
Na svědomí je mají ozbrojení teroristé.
Odpovědností státu je chránit civilisty a státní majetek.
Budeme postupovat pevně a rozhodně.
Režim prezidenta Asada posílá na sever tanky i elitní Republikánskou gardu, které velí jeho bratr Maher.
Bojuje se i u města Džisr aš-Šughúr.
Státní televize opět mluví o vraždění vojáků a policistů.
Za poslední dva dny padlo podle státních médií na sto dvacet příslušníků bezpečnostních sil.
Ozbrojenci přijeli na motorkách, okolo v horách jsou ostřelovači.
Báli jsme se vyjít z domů.
Pořád jsme slyšeli střelbu.
Podle opozice armáda ve městě zabila nejméně pět a třicet lidí.
Kdo může, tak z padesáti-tisícového města utíká.
Západní tlak na Damašek narůstá.
Situace je naprosto jasná, reformy v Sýrii selhaly a podle nás ztratil prezident Asad právo na vládu.
Francie, někdejší obchodní partner rodiny Asadů, chce v Radě bezpečnosti OSN projednat tvrdou rezoluci proti Sýrii.
Pokus bude ale pravděpodobně vetovat Rusko.
Davide Miřejovský, Česká televize.
Země sedmadvacítky můžou podle návrhu europoslanců zvýšit mýtné pro kamiony.
Norma jim umožňuje započítat i poplatek za znečištění ovzduší a hluk.
Každý ujetý kilometr na dálnicích by se tím prodražil v přepočtu asi o korunu. Česko zatím nic měnit nebude.
Z poškozené jaderné elektrárny Fukušima uniklo dvakrát víc radiace, než japonské úřady původně uváděly.
Celkově se ale zamoření rovná jen patnácti procentům toho černobylského.
Vláda v Tokiu připustila, že země nebyla na nehodu takového rozsahu připravená.
Mexická armáda zničila čtyři narkotanky, které vojáci objevili v sobotu.
Speciálně obrněné kamiony byly určené k pašování drog do Spojených států.
Klimatizované tahače s otvory pro ostřelovače dokázala zničit jen protitanková střela.
Přilákat víc návštěvníků, vyrovnat příjmy a výdaje a těsněji spolupracovat se šéfy sbírek hodlá nový ředitel Národní galerie Vladimír Rösel.
Vzděláním ekonom vystřídal na začátku měsíce Milana Knížáka.
Změnit obraz Národní galerie po vzoru zahraničních institucí, nalákat nové návštěvníky.
Rösel se spoléhá na odborníky a své manažerské schopnosti.
Mělo by to být trochu o tom, aby se Národní galerie odosobnila ve smyslu pozice generálního ředitele a aby právě tito odborní pracovníci měli možnost prezentovat to, pro co studovali.
Mě ty vize stále přijdou velice obecné, ale o tom, co vlastně ta Národní galerie chce dělat, proč to chce dělat, kde vidí nějaké své limity a cíle, čemu by se měla věnovat, o tom padlo velice málo.
V plánu je i přímo ve Veletržním paláci zbudovat experimentální prostor.
Ten by měl propagovat hlavně současné umění.
Přitom současnému umění chce nový ředitel věnovat jen desetinu zájmu, zbytek sbírkám.
Také vidinu velkých mezinárodních výstav Rösel odmítl.
Nevyloučil ale legislativní změny ve statutu Národní galerie.
Odpovědnost za chod instituce by podle něj měla přejít z ministerstva kultury nově na správní radu.
Kolektivní orgán by měl vlastně šanci lépe tvořit s tím, co ten potenciál galerie představuje, tak by to bylo daleko jednoduší.
A hlavně by nebyl, jak se říká, politicky závislý.
Národní galerií v současnosti nejvíce hýbe spor státu a firmy Diag Human, kvůli kterému hrozilo zabavení několika desítek uměleckých děl zapůjčených galerií do zahraničí.
Nový ředitel ale tuto kauzu zatím odmítl komentovat.
Petra Schubertová, Česká televize.
3D tiskárny, které už dnes dokážou vyrobit věrné kopie různých předmětů, by se už brzy mohly uplatnit i v domácnostech.
Zhruba do pěti let si tak sami budeme moct vytisknout třeba úchytky do koupelny nebo jiné drobnosti běžné potřeby.
Už teď umějí zpracovat plasty, kov a pryskyřici.
Badatelé ale hledají další materiály.
Během několika desítek minut vymodelují téměř cokoli.
Stačí zadat příslušné parametry do počítačového programu, do dvou hodin může mít člověk na druhém konci světa stejné brýle jako vy, aniž by odešel z domu.
Když budete mít tuto věc doma, můžete si snadno vyrobit náhradní díl pro stůl nebo houpačku.
Oproti klasickým tiskárnám mají ty 3D jednu velkou výhodu, vše, co vytisknou, je plně funkční.
Jako první věc většina lidí tiskne panáka, aby mohli zapít nově narozenou tiskárnu.
Dnes se 3D tiskárny používají především v automobilovém průmyslu a v designu.
Stojí i desítky milionů korun.
Na trhu se ale před pár lety objevily RepRap tiskárny.
Právě ty mají přesunout nákladnou technologii do domácích podmínek.
RepRap postavíte za třináct tisíc korun a hlavní výhodou je, že je celá open-source, takže kdokoliv může tiskárnu vzít a postavit si ji sám.
Trojrozměrně tiskneme už pět a dvacet let.
Nyní se snažíme 3D tiskárny nabízet běžným zákazníkům.
Do tiskáren se používá hned několik materiálů, samolepící folie, pryskyřice nebo kovový prášek.
To nejrozšířenější dneska je určitě plast, je to ABS-plast, to je taková, taková ta struktura nebo kompozice, který se používá třeba u stavebnice Lego.
Loni se na trhu s trojrozměrnými tiskárnami protočila víc než miliarda dolarů.
V roce dva tisíce dvacet už by to mělo být pětkrát víc.
Jana Horká, Česká televize.
Tolik Události.
Ve sportu recept, jak zvítězit, a přitom nedat ani gól.
Kromě jiného, samozřejmě.
Zítra na shledanou.