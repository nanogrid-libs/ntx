﻿using ntx.api.pipe;
using ntx.api.pipe.events;
using System;
using Dasync.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;
using System.Linq;
namespace test
{
    public class EventsFilter
    {
        [Theory]
        [InlineData(1)]
        [InlineData(28)]
        [InlineData(256)]
        [InlineData(4096)]
        [InlineData(1024*1024)]
        public async System.Threading.Tasks.Task ByteFilterAsync(int chunkSize)
        {

            int size = 1024 * 1024;
            Random rnd = new Random();
            var arr = new byte[size];
            rnd.NextBytes(arr);
            var m = new MemoryStream(arr);
            var audioSource = PipeSource.EventsFromBinaryStream(m,chunkSize);
            var sourceData = await audioSource.ToListAsync();


            for (int i = 0; i < 10; i++)
            {
                
                var from = rnd.Next(size-1);
                var count = rnd.Next(size - from);
                if (i == 0)
                {
                    from = 0;
                    count = arr.Length;
                }

                var orig = arr.Skip(from).Take(count);

                var filtered = new MemoryStream();
                var sink = PipeSink.BinaryChunkStream(filtered);
                await PipeSource.FromList(sourceData)
                    .ViaByteFilter(from, count)
                    .ToByteChunkStream().RunWithSink(sink);

                var res = orig.SequenceEqual(filtered.ToArray());
                Assert.True(res);

            }




        }

    }
}
